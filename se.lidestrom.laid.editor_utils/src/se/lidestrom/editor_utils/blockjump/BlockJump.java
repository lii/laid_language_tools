package se.lidestrom.editor_utils.blockjump;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextSelection;

import se.lidestrom.editor_util.EditorHelper;


/**
 * Contains static methods which implements block jumping.
 */
public class BlockJump {
    private BlockJump() {} // Static class
    
    public static boolean isAllWhiteSpace(int ix, int length, IDocument doc) throws BadLocationException {
        for (int stopIx = ix + length; ix < stopIx; ix++) {
            if (!Character.isWhitespace(doc.getChar(ix))) return false;
        }
        return true;
    }
    
    /**
     * Block jumping method. Updates caret position for document which gave rise
     * to event.
     * @param event Block jump event.
     * @param jumpDownwards Should this jump be upwards of downwards?
     * @param select Should selection be modified or cleared?
     */
    public static void performOperation(ExecutionEvent event, boolean jumpDownwards, boolean select) {
        try {
            EditorHelper helper = EditorHelper.create(event);
            if (!helper.isTextEditor()) {
                return;
            }
            
            int oldCarOffs = helper.getCaretOffset();
            int lineNr = helper.getDocument().getLineOfOffset(oldCarOffs);
            
            // Calc the new selection region
            IRegion reg = jumpDownwards ? calcJumpDestDown(helper.getDocument(), lineNr) : calcJumpDestUp(helper.getDocument(), lineNr);
            
            int oldCol = oldCarOffs - helper.getDocument().getLineInformationOfOffset(oldCarOffs).getOffset(),
                newCarOffs = reg.getOffset() + Math.min(oldCol, reg.getLength());
                
            ITextSelection sel = helper.getSelectionModified();
            if (select) {
                helper.getEditor().selectAndReveal(sel.getOffset(), newCarOffs - oldCarOffs + sel.getLength());
            } else {
                helper.getEditor().selectAndReveal(newCarOffs, 0);
            }
            
        } catch (BadLocationException exc) {
            throw new RuntimeException(exc);
        }
    }
    
    private static IRegion calcJumpDestUp(final IDocument doc, int lineNr) throws BadLocationException {
        IRegion reg;
        
        try {
            // Find first non white space line
            do {
                lineNr--;
                reg = doc.getLineInformation(lineNr);
            } while (isAllWhiteSpace(reg.getOffset(), reg.getLength(), doc));
            
            // Find first white space line
            do {
                lineNr--;
                reg = doc.getLineInformation(lineNr);
            } while (!isAllWhiteSpace(reg.getOffset(), reg.getLength(), doc));
            
            // Uppermost non while space line
            reg = doc.getLineInformation(lineNr + 1);
        } catch (BadLocationException exc) {
            // Walked of start of document, set location to first line
            reg = doc.getLineInformation(0);
        }
        
        return reg;
    }
    
    private static IRegion calcJumpDestDown(final IDocument doc, int lineNr) throws BadLocationException {
        IRegion reg;
        lineNr--; // Dec one because we start with inc one.
        
        try {
            // Find next white space line
            do {
                lineNr++;
                reg = doc.getLineInformation(lineNr);
            } while (!isAllWhiteSpace(reg.getOffset(), reg.getLength(), doc));
            
            // Find first non while space line
            do {
                lineNr++;
                reg = doc.getLineInformation(lineNr);
            } while (isAllWhiteSpace(reg.getOffset(), reg.getLength(), doc));
        } catch (BadLocationException exc) {
            // Walked of end of document, set location to last line
            reg = doc.getLineInformation(doc.getNumberOfLines() - 1);
        }
        
        return reg;
    }
}
