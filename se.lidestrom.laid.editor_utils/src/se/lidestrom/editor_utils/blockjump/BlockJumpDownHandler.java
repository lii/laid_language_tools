package se.lidestrom.editor_utils.blockjump;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;

public class BlockJumpDownHandler extends AbstractHandler {
    @Override
    public Object execute(ExecutionEvent event) {
        BlockJump.performOperation(event, true, false);
        return null;
    }
}
