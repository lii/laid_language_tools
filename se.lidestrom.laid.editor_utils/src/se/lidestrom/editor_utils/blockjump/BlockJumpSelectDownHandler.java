package se.lidestrom.editor_utils.blockjump;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;

public class BlockJumpSelectDownHandler extends AbstractHandler {
    @Override
    public Object execute(ExecutionEvent event) {
        BlockJump.performOperation(event, true, true);
        return null;
    }
}
