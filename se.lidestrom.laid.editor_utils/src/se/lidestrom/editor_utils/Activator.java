package se.lidestrom.editor_utils;

import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {
    
    // The plug-in ID
    public static final String PLUGIN_ID = "se.lidestrom.editor_utils"; //$NON-NLS-1$
    
    // The shared instance
    private static Activator plugin;
    
    /**
     * The constructor
     */
    public Activator() {}

    @Override
    public void start(BundleContext context) throws Exception {
        super.start(context);
        plugin = this;
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        plugin = null;
        super.stop(context);
    }
    
    /**
     * Returns the shared instance
     */
    public static Activator getDefault() {
        return plugin;
    }
    
    /**
     * Returns an image descriptor for the image file at the given plug-in
     * relative path
     */
    public static ImageDescriptor getImageDescriptor(String path) {
        return imageDescriptorFromPlugin(PLUGIN_ID, path);
    }
    
    public static void logWarning(String msg) {
        log(Status.WARNING, msg);
    }
    
    public static void logError(String msg) {
        log(Status.ERROR, msg);
    }
    
    public static void log(int status, String msg) {
        getDefault().getLog().log(new Status(status, PLUGIN_ID, msg));
    }
}
