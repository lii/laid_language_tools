
package se.lidestrom.editor_utils.scrollwithcaret;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;

public class ScrollWithCaretDownHandler extends AbstractHandler {
    
    @Override
    public Object execute(ExecutionEvent e) {
        ScrollWithCaret.handle("org.eclipse.ui.edit.text.scroll.lineDown", "org.eclipse.ui.edit.text.goto.lineDown");
        
        return null;
    }
    
}