package se.lidestrom.editor_utils.scrollwithcaret;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.NotEnabledException;
import org.eclipse.core.commands.NotHandledException;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.ui.handlers.IHandlerService;

import se.lidestrom.editor_util.EditorHelper;

public class ScrollWithCaret {
    public static void handle(String scrollCommand, String gotoCommand) {
        EditorHelper helper = EditorHelper.createForActiveEditor();
        if (!helper.isTextEditor()) {
            // TODO: Log
            return;
        }
        IHandlerService handlerService = helper.getHandlerService();
        try {
            handlerService.executeCommand(scrollCommand, null);
            handlerService.executeCommand(gotoCommand, null);
        } catch (ExecutionException | NotDefinedException | NotEnabledException | NotHandledException exc) {
            throw new RuntimeException(exc);
        }
    }
}
