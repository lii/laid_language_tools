
package se.lidestrom.editor_utils.scrollwithcaret;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;

public class ScrollWithCaretUpHandler extends AbstractHandler {
    @Override
    public Object execute(ExecutionEvent e) {
        ScrollWithCaret.handle("org.eclipse.ui.edit.text.scroll.lineUp", "org.eclipse.ui.edit.text.goto.lineUp");
        
        return null;
    }
}