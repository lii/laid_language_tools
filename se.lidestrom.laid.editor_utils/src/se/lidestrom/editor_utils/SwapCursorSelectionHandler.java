package se.lidestrom.editor_utils;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.jface.text.ITextSelection;

import se.lidestrom.editor_util.EditorHelper;

public class SwapCursorSelectionHandler extends AbstractHandler {
    @Override
    public Object execute(ExecutionEvent event) {
        EditorHelper helper = EditorHelper.create(event);
        if (!helper.isTextEditor()) {
            // TODO: Log
            return null;
        }
        ITextSelection sel = helper.getSelectionModified();
        helper.getEditor().selectAndReveal(sel.getOffset() + sel.getLength(), -sel.getLength());
        
        return null;
    }
}


