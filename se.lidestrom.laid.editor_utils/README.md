# Editor Utils

This project contains an Eclipse plug-in  that adds a few simple commands for text editors.

* **Swap Cursor Selection:** Moves the cursor to the other end of a selected block of text, without changing the selection.
* **Block Jump Up/Down:** Moves the cursor the the next block of lines that is separated from other block with blank lines. This can be used a little bit like the command *Go to Next/Previous Member* it JDT.
* **Scroll With Caret Line Up/Down**: Moves the cursor one line and scrolls the editor at the same time, leaving the cursor at the same position on the screen.

## Installation

See instructions located in the root directory of this repository.


## Author

Jens Lideström
