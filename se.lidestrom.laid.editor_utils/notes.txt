Contributor (BasicTextEditorActionContributor) creates RetargetTextEditorAction with an ID string: IJavaEditorActionDefinitionIds.SELECT_ENCLOSING.

JavaEditor creates the real StructureSelectEnclosingAction with same the ID.

When the command in issued the retarget action probably is calling the real one. 

    action = new StructureSelectEnclosingAction(this, fSelectionHistory);
    action.setActionDefinitionId(IJavaEditorActionDefinitionIds.SELECT_ENCLOSING);
    setAction(StructureSelectionAction.ENCLOSING, action);
    
    fStructureSelectEnclosingAction = new RetargetTextEditorAction(b, "StructureSelectEnclosing."); //$NON-NLS-1$
    fStructureSelectEnclosingAction.setActionDefinitionId(IJavaEditorActionDefinitionIds.SELECT_ENCLOSING);

