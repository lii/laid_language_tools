
# The Laid Project

The overall goal of this project is to make Eclipse a better tool for working with languages that don't have advanced specific plug-ins. This is archived by:

* Creating plug-is that add commands which make the Eclipse editors work better for those languages.
* Creating a build output parser that creates problem markers for external build tools. 
* Describing how to use third party plug-ins for that provide for example syntax coloring for new languages.
* Describing how use standard Eclipse features for working with new languages.

This document gives an overview of the plug-ins within the project. For more detailed information about them see  read-me files in the specific plug-in sub-directories.

If these plug-ins are used to get support for a new language then see the document `Setting_up_Eclipse_for_a_new_language.md` for more information.

Laid stands for Language Aid.


## Plug-ins 

### Build Error Marker

This plug-in creates Eclipse problem markers for editors and the Problems view by scanning the output from external program builders. The output is parsed by a user provided parser class. A regex based parser is included in the plug-in.


### Editor Utilities Plug-in

Contains a number of small additions to text editors.

* **Block Jump Up/Down:** Moves the cursor the the next block of lines that is separated from other block with blank lines.
* **Swap Cursor Selection:** Moves the cursor to the other end of a selected block of text, without changing the selection.
* **Scroll With Caret Line Up/Down**: Moves the cursor one line and scrolls the editor at the same time, leaving the cursor at the same position on the screen.


### Select Marker Block Plug-in

This plug-in provides a generic way to select text that is contained inside different kinds to brackets. The plug-in works by searching for start/end marks using regular expressions. Support for markers for more languages can be added using a configuration file.

It comes bundled with support for C/Java-like language, Haskell and Lisp. Additional languages can be added easily by creating a language marker description file.


## Installation

* Use the project update site to install all or some of the plug-ins using the Eclipse *Install New Software" feature. The update site is located at `https://bitbucket.org/lii/laid_language_tools/raw/master/se.lidestrom.laid.update_site/`.

## More about the Laid project

Right now the plug-ins in my project, together with a colorer plug-in and standard Eclipse features, enables a user to get much useful functionality for a new language. But it requires some effort to set up this, several separate plug-ins need to be installed and configured, and builders and launcher need to be added.) I would also like to create a framework for language designers, with minimal effort, to create custom plug-ins for their languages.

### Related projects

This project has a similar goal as the Eclipse frameworks MelnormeEclipse, Dynamic Languages Toolkit and Xtext. But these projects aims to help to make more advanced language plug-ins. The Laid project aims to create a minimal effort solution to get only some basic functionally for new languages. It is thus quicker and easier to use.


## Building

Clone the repository, import the projects into Eclipse.


## Dependencies

Java 8 and Eclipse 4.0. 

No third-party libraries have been used.


## To-do

Currently none.


## Ideas

* A plug-in framework that enables language authors to create custom language plug-ins. The most important features are: 
    - To install language support plug-ins using a single update site. These include:
      * Syntax colorer
      * Build problem marker
      * Other language specific plug-ins?
    - A configuration command to add support for a new language to an Eclipse project
      * Install builder and problem markers
      * Install launchers
      * Install other resources?

* A way to generate and use ctags in a language generic way. Does LiClipse do this?

* Language generic debugger integration. Maybe using CDT's GDB tools.





