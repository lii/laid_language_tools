
### Files with keyboard shortcuts
    workspace/.metadata/.plugins/org.eclipse.core.runtime/.settings/org.eclipse.ui.workbench.prefs
    workspace/.metadata/.plugins/org.eclipse.e4.workbench/workbench.xmi

### Command line to uninstall feature
    eclipse -application org.eclipse.equinox.p2.director -uninstallIU se.lidestrom.laid.build_marker_feature.feature.group/1.0.3
    
List features:

    eclipse -application org.eclipse.equinox.p2.director -listInstalledRoots


## Should Colorer or LiClipse be prefered for new languages?

Pro Colorer:

* Many languages
* Nicer colors
* I have more knowledge about it
* Faster?
* Already have nice Haskell colors
* Colors can be configured better

Pro LiClipse

* Easier language definition?
* Pure Java
* Actively maintained
* More features: Select markers, comment, quick outline
* Easier to work with external language definitions
* More robust algorithm, doesn't lose colours as often



## To-do in LiClipse

### Feature suggestion: *Select Next/Previous Scope*

The *Select Enclosing Scope* command very useful. *Select Next/Previous Scope* commands would also be useful. They would expand the selection to contain the next/previous scope on the *same* level as the cursor position. Compare with the JDT *Select Next/Previous Element* commands which do a similar thing.

A slightly related remark: The names of LiClipseText selection commands don't match the names of the JDT commands that do similar things. The LiClipseText *Next Element* command is similar to the JDT *Go to Next Member*, not to the JDT *Select Next Element*.

Maybe it would be a good idea to change the names of these commands in LiClipse to *Select Enclosing/Next/Previous Element*, and the *Next/Previous Element* to *Go to Next/Previous Member* to be consistent with JDT (and also with CDT).

### Feature suggestion: Contribute languages though an extension point, and move bundled languages to a separate plug-in

#### Contribute language directory through an extension point

It would sometimes be useful to be able to contribute a language directory through an extension point, instead of placing the language file in a tracked language directory. This is the case for language designers who wish to use LiClipseText as a component for a plug-in for their specific language.

The contributed directory could be tracked just as other languages directories.

#### Move the bundled languages to a separate plug-in

The other part of this suggestion is useful in the same situation. When used for syntax colors (and other functionality) for one specific language I think it is to disruptive for the user to also get syntax colors for a bunch of other languages.

This language-bundle plug-in could make the languages available by contributing them to the extension point.

There could then be three different Eclipse features for LiClipseText. 

1. One standard which include bundled languages.
2. One called LiClipseText Core (or something like that) which don't come bundled with languages.
3. One called LiClipseText Language Bundle (or something like that).

### Background and motivation

I have for some time had a low intensity project that aims to create a minimal effort way to use Eclipse with new languages, which don't have advanced plug-ins available. The suggestions above aims to help with that project.

I have so far created a plug-in which lets user get problem markers from compiler output for compilers that are run as external program builders. It is [available on Bit Bucket][marker_page]. (I have also created [some][marker_selection_page] [smaller][editor_utils_page] plug-ins, but most of the functionality they provide seem to be available in LiClipseText.)

Right now my plug-ins, together with a colorer plug-in and standard Eclipse features, enables a user to get much useful functionality for a new language. But it requires some effort to set up this, several separate plug-ins need to be installed and configured, and builders and launcher need to be added.) I would also like to create a framework for language designers, with minimal effort, to create custom plug-ins for their languages. LiClipseText could be the most important part of such a framework, and the features suggested here aims to enable that.


### Other


* It would also be useful to be able to install LiClipseText with support only for one selected language. This could be done be done by separating the code and the language files into to plug-ins, and provide a feature to install them together.

  => This is useful when a language author wants to use LiClipse to create a plug-in for their language.

* I don't think LiClipse should set itself as the default editor. When there is a more specific editor available that is probably preferred. Example: The standard XML editor. TODO: Does it?

* Is it necessary to create one editor type for every supported language? It is a bit confusing with so many LiClipse editors displayed in the GUI.


[marker_page]: https://bitbucket.org/lii/laid_language_tools/src/master/se.lidestrom.laid.build_marker/
[marker_selection_page]: https://bitbucket.org/lii/laid_language_tools/src/master/se.lidestrom.laid.se.lidestrom.laid.marker_block_selection/
[editor_utils_page]: https://bitbucket.org/lii/laid_language_tools/src/master/se.lidestrom.laid.editor_utils
