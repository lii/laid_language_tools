# Setting up Eclipse for a new language

This document describes how an user can set up Eclipse for working with a new language. This is done using some plug-ins that are developed for this project, some standard Eclipse features and some third-party plug-ins.

The things that can be added include: support for syntax colors, editor navigation, building programs and running programs from within Eclipse.

The Laid Project plug-ins can be installed from the [project update site][update_site].

## Overview

The following is a list of plug-ins and features which can be useful for working with a new languages.

* **Syntax colors** can be added with either the [Eclipse Colorer][colorer_page] or [LiClipse][liclipse_page]. They are plug-ins that provides syntax colors for a large number of languages. New languages can be added easily using custom color files.

* To **build** using an external compiler from inside Eclipse the *Program Builder* feature can be used. The builder is run when the normal *Build Project* command is issued. To set up a new builder: *Project Properties -> Builders -> New... -> Program*

* To **capture compile errors and warning** from an external builder a build output parser can be used. [Laid Builder Marker][build_marker_page] is a plug-in that implements such parsers. The parsers put problem markers in editors and in the Problems view. This plug-in can also be used to add Program Builders to projects.

* To **run** compiled programs from inside Eclipse the use the *External Tools* feature. (*Run* -> *External Tools*)

* Extended support for **code navigation and text selection** can be added with some of the other Laid Project plug-ins: The [Marker Block Selection][marker_block_page] plug-in can be used to select text enclosed be brackets. The [Editor Utilities][editor_utils_page] plug-in can be used to, among other things, jump between blocks of code delimited by blank lines.

* Use the standard *Word Completion* command (*Shift+Alt+7*) as a poor person's content assist.

* Use the standard *Toggle Block Selection* command (Shift+Alt+A) to insert/remove line comments on multiple lines at the same time.

* Some other useful standard Eclipse features include:
  * The *Open Resource* command (*Ctrl+Shift+R*)
  * The *File Search* command (*Ctrl+H*)
  * The Bookmarks feature (*Edit -> Add Bookmark*). Make sure to check *Include in next/prev navigation* box (*Preferences -> General -> Editors -> Text Editors -> Annotations -> Bookmarks*).

## Syntax colors

[**Eclipse Colorer**][colorer_page] and [**LiClipseText**][liclipse_page] are two third party plug-ins that provide syntax colors for a large number of languages. They also allow users to add new languages easily using custom color files. Both seem to be working well.

The two has some advantages and disadvantages compared to each other:

* **LiClipseText** provides more commands (*Quick Outline*, *Select Enclosing Block*, *Toggle Comment*), it allows new languages without modifying the plug-in directory, it is written entirely in Java, and it seems to be actively maintained. I think this is the better choice for new languages.

* **Eclipse Colorer** comes bundled with support for more languages. Its language definition files are well documented, they seem to have more functionality but also to be more complex. It is partly written in C++ and the development seems to have stalled.


### LiClipseText plug-in

Documentation can be found on its [home page][liclipse_page] and it can be installed from its [update site][liclipse_update_site].

### Eclipse Colorer plug-in

Documentation can be found on its [home page][colorer_page] and it can be installed from its [update site][colorer_update_site].

It is easy to create a simple syntax file for a new language. The file `example_lang.hrc` demonstrates this, and contains information about how a file is registered with the Colorer plug-in.

## Tips and tricks

### Workspace variables

The Eclipse workspace variables are in very handy and can be used for most of the values in builder and parser configurations. For example `${build_project}` refers to the absolute path of the project being build, and `${resource_name}` to the name of the selected file. Learn more about them in the [External Tools section][tools_help_page] of the Eclipse help system.

The workspace variables can, for example, be used to create a builder that invokes a compiler with the selected file, or an external tool launcher which runs the selected file.


[tools_help_page]: http://help.eclipse.org/juno/topic/org.eclipse.platform.doc.user/concepts/concepts-exttools.htm
[colorer_page]: http://colorer.sourceforge.net/eclipsecolorer/
[colorer_update_site]: http://colorer.sourceforge.net/eclipsecolorer/
[liclipse_page]: http://www.liclipse.com/text
[liclipse_update_site]: http://www.liclipse.com/text/updates
[build_marker_page]: https://bitbucket.org/lii/laid_language_tools/src/HEAD/se.lidestrom.laid.build_marker
[editor_utils_page]: https://bitbucket.org/lii/laid_language_tools/src/HEAD/se.lidestrom.laid.editor_utils
[marker_block_page]: https://bitbucket.org/lii/laid_language_tools/src/HEAD/se.lidestrom.laid.marker_block_selection
[update_site]: https://bitbucket.org/lii/laid_language_tools/src/master/se.lidestrom.laid.update_site/

