# Marker Block Selection Plug-in

This plug-in provides a generic way to select text that is contained inside different kinds to brackets, for example inside parenthesis.  It works by searching for start/end marks using regular expressions.

It comes bundled with support for C/Java-like languages, Haskell and Lisp. Support for more languages can be added easily by creating a language marker description file.


## Installation

See instructions located in the root directory of this repository.


## Markers

The term *marker* is used to refer to a type of text token on which the selection commands operate. The most obvious example is the pair of opening and closing parenthesis. 

Markers and be of two kinds: *Start-end markers* matches start and end sections separately. These can be nested in any way. An example of this is parentheses. *Region markers* matches one section of text. They can not be nested. An example of this is strings enclosed in `"` characters.


## Commands

This plug-in provides four commands for working with text selection:

* Select Enclosing Marker Block
* Select Next Marker Block
* Select Previous Marker Block
* Restore Marker Block


## Language configuration files

The set of markers that are used for a certain file type is configured using regular expressions in a XML language configuration file.

See the bundled file `res/languages.xml` for an annotated example on how this is done.

Additional language files can be added in two ways:

* By setting the workspace variable `se.lidestrom.laid.marker_block_selection.languageDir` to a directory. All `.xml` files in this directory will be interpreted as language files and their content will be loaded into the plug-in. This workspace variable can be set for example in *Preferences* -> *Run/Debug* -> *String Substitutions*. (This slightly peculiar configuration method is used only because no proper solution has yet been implemented.)

* By contributing a language file path to the `se.lidestrom.laid.editor_utils.languages` extension point.


## Author

Jens Lideström

