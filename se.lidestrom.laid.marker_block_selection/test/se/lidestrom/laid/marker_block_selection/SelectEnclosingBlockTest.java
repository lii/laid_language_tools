package se.lidestrom.laid.marker_block_selection;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.Region;
import org.junit.Test;

import se.lidestrom.editor_util.EditorUtils;
import se.lidestrom.laid.marker_block_selection.internal.Marker;
import se.lidestrom.laid.marker_block_selection.internal.SelectMarkerBlock;

public class SelectEnclosingBlockTest {

    private final List<Marker> parentsMarkerList = Arrays.asList(new Marker("parens", "\\(", "\\)"));
    private final Marker parentsMarkerCombined = Marker.makeCombined(parentsMarkerList);

    private final List<Marker> threeMarkers = Arrays.asList(
        new Marker("round", "\\(", "\\)"), new Marker("square", "\\[", "\\]"), new Marker("curly", "\\{", "\\}"));

    private final Marker threeMarkersCombined = Marker.makeCombined(threeMarkers);
    private final Marker stringMarker = new Marker("string", LanguageFileGenerator.STRING_PATTERN);
    
    private final List<Marker> wordParenMarkers = Arrays.asList(new Marker("word", "\\w+"), new Marker("round", "\\(", "\\)"));
    private final Marker wordParenMarkerCombined = Marker.makeCombined(wordParenMarkers);
    
    public void printDoc(IDocument doc) throws BadLocationException {
      for (int i = 0; i < doc.getLength(); i++) {
          System.out.format("%2d: %s %n", i, doc.getChar(i));
      }
    }
    
    @Test
    public void simpleSameLine() throws BadLocationException {
        Document doc = new Document(
            "text text\n" +
            "t(x( )ext )ext\n" +
            "text text\n");
        
        IRegion s = SelectMarkerBlock.selectEnclosing(
            doc, new Region(17, 0), parentsMarkerCombined, parentsMarkerList).get();
        
        assertEquals(11, s.getOffset());
        assertEquals(10, s.getLength());
    }

    @Test
    public void differentLine() throws BadLocationException {
        Document doc = new Document(
            "text ((xt\n" +
            "text text\n" +
            "text )e)t\n");
        
        IRegion s = SelectMarkerBlock.selectEnclosing(
            doc, new Region(12, 0), parentsMarkerCombined, parentsMarkerList).get();
        
        assertEquals(6, s.getOffset());
        assertEquals(20, s.getLength());
    }

    @Test
    public void threeMarkersAndMarkerInsideSelection() throws BadLocationException {
        Document doc = new Document("te{t (ex[ te]t t)xt text t}xt\n");
        IRegion s = SelectMarkerBlock.selectEnclosing(
            doc, new Region(11, 3), threeMarkersCombined, threeMarkers).get();
        
        assertEquals(5, s.getOffset());
        assertEquals(12, s.getLength());
    }

    @Test
    public void closestMarkers() throws BadLocationException {
        Document doc = new Document(
            "te{t [ext tex( t}x] )ext");
        
        IRegion s = SelectMarkerBlock.selectEnclosing(
            doc, new Region(14, 0), threeMarkersCombined, threeMarkers).get();

        
        assertEquals(13, s.getOffset());
        assertEquals(8, s.getLength());

        s = SelectMarkerBlock.selectEnclosing(
            doc, new Region(7, 0), threeMarkersCombined, threeMarkers).get();

        assertEquals(5, s.getOffset());
        assertEquals(14, s.getLength());
    }
    
    @Test
    public void overlapping() throws BadLocationException {
        Document doc = new Document("( ( ) )");
        
        IRegion s = SelectMarkerBlock.selectEnclosing(
            doc, new Region(2, 2), parentsMarkerCombined, parentsMarkerList).get();
        
        assertEquals(0, s.getOffset());
        assertEquals(7, s.getLength());
    }
    
    @Test
    public void unbalancedMarkers() throws BadLocationException {
        IRegion r1 = SelectMarkerBlock.selectEnclosing(
            new Document("[  { [ ] ( ) ] }"), 
            new Region(5, 2), 
            threeMarkersCombined, 
            threeMarkers).get();
        
        assertEquals(3, r1.getOffset());
        assertEquals(13, r1.getLength());
        
        IRegion r2 = SelectMarkerBlock.selectEnclosing(
            new Document("{ [ ] ( ) ] }"), 
            new Region(5, 2), 
            threeMarkersCombined, 
            threeMarkers).get();
        
        assertEquals(0, r2.getOffset());
        assertEquals(13, r2.getLength());
        
        // Matching pair is not nearest cursor in any direction
        IRegion r3 = SelectMarkerBlock.selectEnclosing(
            new Document(" [   ( {  ] )   }  "), 
            new Region(9, 0), 
            threeMarkersCombined, 
            threeMarkers).get();
        
        assertEquals(5, r3.getOffset());
        assertEquals(8, r3.getLength());
    }

    @Test
    public void stringsEscape() throws BadLocationException {
        IRegion s = SelectMarkerBlock.selectEnclosing(
            new Document("a\"b\\\"c\"d"), new Region(2, 1), 
            stringMarker, 
            Arrays.asList(stringMarker)).get();
        
        assertEquals(1, s.getOffset());
        assertEquals(6, s.getLength());
    }

    @Test
    public void stringsBasic() throws BadLocationException {
        IRegion s = SelectMarkerBlock.selectEnclosing(
            new Document("a\"bc\"d"), new Region(2, 1), 
            stringMarker, 
            Arrays.asList(stringMarker)).get();
        
        assertEquals(1, s.getOffset());
        assertEquals(4, s.getLength());
    }
    
    @Test
    public void nextMatchSimple() throws BadLocationException {
        IRegion s = SelectMarkerBlock.selectNext(
            new Document("  ( ) "), 
            new Region(1, 0), parentsMarkerCombined, parentsMarkerList).get();
        
        assertEquals(1, s.getOffset());
        assertEquals(4, s.getLength());
    }

    @Test
    public void nextMatchId() throws BadLocationException {
        IRegion s = SelectMarkerBlock.selectNext(
            new Document("  a "), 
            new Region(1, 0), wordParenMarkerCombined, wordParenMarkers).get();
        
        assertEquals(1, s.getOffset());
        assertEquals(2, s.getLength());
    }

    @Test
    public void enclosingMarkerAtEndOfRegion() throws BadLocationException {
        Optional<IRegion> s = SelectMarkerBlock.selectEnclosing(
            new Document(" a "), 
            new Region(2, 0), wordParenMarkerCombined, wordParenMarkers);

        assertFalse(s.isPresent());

//        assertEquals(1, s.getOffset());
//        assertEquals(1, s.getLength());
    }

    @Test
    public void nextMatchIdAtPrevious() throws BadLocationException {
        IRegion s = SelectMarkerBlock.selectNext(
            new Document(" a a "), 
            new Region(2, 0), wordParenMarkerCombined, wordParenMarkers).get();
        
        assertEquals(2, s.getOffset());
        assertEquals(2, s.getLength());
    }

    @Test
    public void nextMatchIdParents() throws BadLocationException {
        IRegion s = SelectMarkerBlock.selectNext(
            new Document(" () "), 
            new Region(1, 0), parentsMarkerCombined, parentsMarkerList).get();
        
        assertEquals(1, s.getOffset());
        assertEquals(2, s.getLength());
    }
    
    @Test
    public void previousMatchId() throws BadLocationException {
        IRegion s = SelectMarkerBlock.selectPrevious(
            new Document(" a  "), 
            new Region(3, 0), wordParenMarkerCombined, wordParenMarkers).get();
        
        assertEquals(1, s.getOffset());
        assertEquals(2, s.getLength());
    }
    
    @Test
    public void previousMatchSimple() throws BadLocationException {
        IRegion s = SelectMarkerBlock.selectPrevious(
            new Document(" ( )  "), 
            new Region(5, 0), parentsMarkerCombined, parentsMarkerList).get();
        
        assertEquals(1, s.getOffset());
        assertEquals(4, s.getLength());
    }
    
    @Test
    public void streamFor() {
        assertEquals(
            Arrays.asList("0", "1", "2"), 
            EditorUtils.streamFor(
                0, i -> i < 3, i -> i + 1, Object::toString).collect(toList()));
        
        assertEquals(
            Arrays.asList(), 
            EditorUtils.streamFor(
                3, i -> i < 3, i -> i + 1, Object::toString).collect(toList()));
    }

    
    @Test
    public void markerPatterns() {
        String p = LanguageFileGenerator.CHAR_PATTERN;
        assertMatch(p, " 'c' "   , "'c'"   );
        assertMatch(p, " '\\'' " , "'\\''" );  
        assertMatch(p, " '\\\\' ", "'\\\\'");
        assertMatch(p, " ''' "   , null    );
        assertMatch(p, " '\\' "  , null    );   
    }

    
    public static void assertMatch(String pattern, String input, String expected) {
        Pattern p = Pattern.compile(pattern);
        
        Matcher m = p.matcher(input);
        boolean found = m.find();
        
        if (expected == null) {
            assertFalse(found);
        
        } else {
            assertTrue(found);
            assertEquals(expected, m.group());
        }
    }
}
