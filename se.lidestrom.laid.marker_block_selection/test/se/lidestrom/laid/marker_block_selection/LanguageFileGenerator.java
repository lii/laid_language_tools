package se.lidestrom.laid.marker_block_selection;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import se.lidestrom.laid.marker_block_selection.LanguageContainer.Languages;
import se.lidestrom.laid.marker_block_selection.internal.Marker;

public class LanguageFileGenerator {
    public static final String STRING_PATTERN = "\"(?:\\\\[\\\\'\"tnbfru01234567]|[^\\\\\"])*?\"";
    public static final String CHAR_PATTERN = "'((\\\\.)|(\\\\u\\d+)|([^\\\\']))'";
    public static final String HASKELL_OPERATOR_PATTERN = "[\\Q<$>+-*:#!/&|\\E]+";
    
    // Default default language contains nothing
    private static Language defaultLanguage // = new Language("empty", null, "");
        = new Language("default", null, "",
            new Marker("default-string",        STRING_PATTERN),
            new Marker("default-char",          CHAR_PATTERN),
            new Marker("default-identifier",    "\\p{javaJavaIdentifierStart}\\p{javaJavaIdentifierPart}*(\\.\\p{javaJavaIdentifierStart}\\p{javaJavaIdentifierPart}*)*"),
            new Marker("default-square-brackets", "\\[", "\\]"),
            new Marker("default-round-brackets",  "\\(", "\\)"),
            new Marker("default-curly-brackets",  "\\{", "\\}"));
    
    private static List<Language> languages = Arrays.asList(
        new Language("haskell", "default", ".hs|.lhs",
            new Marker("haskell-line-comment", "--\\^? .*"),
            new Marker("haskell-operators", HASKELL_OPERATOR_PATTERN)),
        new Language("java", "default", ".java",
            new Marker("java-line-comment", "//.*")));
    
    public static void writeLanguages() throws JAXBException {
        Languages config = new Languages();
        config.languages.add(defaultLanguage);
        config.languages.addAll(languages);
        
        Marshaller marshaller =
            JAXBContext.newInstance(Languages.class).createMarshaller();
        
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        
        marshaller.marshal(config, new File("res/languages.xml"));
    }
    
    public static void main(String[] args) throws JAXBException {
        writeLanguages();
    }

}



