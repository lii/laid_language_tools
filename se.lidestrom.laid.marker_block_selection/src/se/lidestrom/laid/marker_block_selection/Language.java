package se.lidestrom.laid.marker_block_selection;

import static java.util.stream.Collectors.toCollection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import se.lidestrom.editor_util.LazyValue;
import se.lidestrom.editor_util.OptionalAdapter;
import se.lidestrom.laid.marker_block_selection.internal.Marker;

/**
 * Information about one language. That is, its markers, its name and its parent name.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Language {

    public static class StringOptionalAdapter extends OptionalAdapter<String> {}
    
    @XmlAttribute(required = true)
    private String id;
    
    @XmlAttribute
    private String extensions;
    
    @XmlElementWrapper
    @XmlElement(name = "contentType")
    private List<String> contentTypes = new ArrayList<>();
    
    @XmlAttribute
    @XmlJavaTypeAdapter(StringOptionalAdapter.class)
    private Optional<String> parent = Optional.empty();
    
    // Only markers directly specified by this language
    @XmlElementWrapper
    @XmlElement(name = "marker", required = true)
    private List<Marker> markers;
    
    // All languages is the system. Used to lookup parents.
    @XmlTransient 
    private Map<String, Language> allLanguages = null;
    
    // Markers, including parents markers.
    @XmlTransient
    private LazyValue<Set<Marker>> allMarkers = new LazyValue<Set<Marker>>(() -> {
            Optional<Stream<Marker>> parentMarkers = parent
                .map(p -> allLanguages.computeIfAbsent(parent.get(),
                    _key -> { throw new IllegalStateException("Unknown parent for " + id + ": " + parent.get()); }))
                .map(l -> l.getMarkers().stream().filter(m -> !markers.contains(m)));
            
            return Stream.concat(markers.stream(), parentMarkers.orElse(Stream.empty()))
                .collect(toCollection(LinkedHashSet::new));
        }
    );
    
    // Initialised in addParentMarkers
    @XmlTransient
    private LazyValue<Marker> combinedMarker = new LazyValue<>(() -> Marker.makeCombined(allMarkers.get()));

    public Language(String name, String parent, String suffix, Marker... directMarkers) {
        this.id = name;
        this.parent = Optional.ofNullable(parent);
        this.extensions = suffix;
        this.markers = Arrays.asList(directMarkers);
    }

    // For XML deserialisation
    @SuppressWarnings("unused")
    private Language() {}

    public Collection<String> getExtensions() {
        if (extensions == null || extensions.matches("\\w*")) return Collections.emptyList();
        return Arrays.asList(extensions.split("\\|"));
    }
    
    /**
     * Recursive functions which adds all parents markers to this one. And adds grand parents
     * markers to parents markers, and so and. 
     * 
     * @param allLanguages For looking up parents (by name)
     */

    public String getId() {
        return id;
    }
    
    public Collection<Marker> getMarkers() {
        return allMarkers.get();
    }
    
    public Marker getCombinedMarker() {
        return combinedMarker.get();
    }

    public List<String> getContentTypes() {
        return contentTypes;
    }

    @Override
    public String toString() {
        return "<Language id=" + id + ", extensions=" + extensions + ", parent=" + parent + ">";
    }

    public void setAllLanguages(Map<String, Language> nameLanguageMap) {
        allLanguages = nameLanguageMap;
    }
}
