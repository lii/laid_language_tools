package se.lidestrom.laid.marker_block_selection;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static se.lidestrom.editor_util.EditorUtils.toLinkedHashMap;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.core.variables.IValueVariable;
import org.eclipse.core.variables.VariablesPlugin;
import org.eclipse.ui.IEditorPart;

import se.lidestrom.editor_util.EditorUtils;
import se.lidestrom.editor_util.FunctionUtils;
import se.lidestrom.editor_util.JaxbPostConstructRunner;

/**
 * Loads and keeps information about markers for different languages.
 */
public class LanguageContainer {
    
    // Files get this is there is no matching suffix
    private Optional<Language> defaultLanguage;
    // Maps file suffix to the language to be used
    private Map<String, Language> suffixLanguageMap;
    private Map<IContentType, Language> contentTypeLanguageMap;
    // Tracks whether lazy initialisation has been performed
    private boolean hasBeenInitialised = false;
    
    /**
     * Returns the languages to be used by the editor. First ties content type of editor.
     * Then file extension. If editor has not associated file, try editor name.
     */
    public Optional<Language> getLanguageForEditor(IEditorPart editor) {
        if (!hasBeenInitialised) {
            configLanguages(readLanguages());
        }
        
        String fileName = EditorUtils.getEditorFile(editor).map(IFile::toString)
            .orElse(null);
        
        // Handle non-file editor where we have to use the name
        if (fileName == null) {
            String suffix = EditorUtils.fileExtension(editor.getEditorInput().getName());
            return suffix.isEmpty() ? defaultLanguage : lookupLanguageOrDefualt(suffix);
        }

        // Here we have a file editor for which we should be able to get the file

        Language languageOfContentType = contentTypeLanguageMap.get(
            Platform.getContentTypeManager().findContentTypeFor(fileName));
        
        if (languageOfContentType != null) {
            return Optional.of(languageOfContentType);
        }

        return lookupLanguageOrDefualt(EditorUtils.fileExtension(fileName));
    }

    private Optional<Language> lookupLanguageOrDefualt(String fileExtension) {
        return Optional.ofNullable(suffixLanguageMap.getOrDefault(
            fileExtension, defaultLanguage.orElse(null)));
    }

    private List<Language> readLanguages() {
        IConfigurationElement[] configElems = Platform.getExtensionRegistry()
            .getConfigurationElementsFor(SelectMarkerBlockPlugin.LANGUAGES_EXTENTION_POINT_ID);
        
        // Languages from extension point contributions
        Stream<SourceAndStream> pluginContributionFileStreams = Stream.of(configElems)
            .map(configElem -> new SourceAndStream(
                configElem.getAttribute("path"),
                FunctionUtils.runtimizing(() -> FileLocator.openStream(
                    Platform.getBundle(configElem.getNamespaceIdentifier()),
                    new Path(configElem.getAttribute("path")), false))));
        
        // Dir referred to by workspace variable
        Optional<java.nio.file.Path> langDir = Optional.ofNullable(
                VariablesPlugin.getDefault().getStringVariableManager()
                    .getValueVariable(SelectMarkerBlockPlugin.LANGUAGE_DIR_VAR_NAME))
                .map(IValueVariable::getValue)
            .flatMap(p -> {
                try {
                    return Optional.of(VariablesPlugin.getDefault()
                        .getStringVariableManager().performStringSubstitution(p));
                } catch (CoreException e) {
                    SelectMarkerBlockPlugin.logWarning("Variable lookup exception: " + e);
                    return Optional.empty();
                }
            })
            // TODO: This does not handle workspace links 
            .map(Paths::get);
        
        List<SourceAndStream> langDirFileStreams = emptyList();
        
        // Languages from this dir
        if (langDir.isPresent()) {
            if (!Files.exists(langDir.get())) {
                SelectMarkerBlockPlugin.logWarning("Language dir variable does not refer to a directory: " + langDir.get());
            } else {
                langDirFileStreams = langDir
                    .map(Stream::of)
                    .orElse(Stream.empty())
                    .flatMap(FunctionUtils.runtimizing(Files::walk))
                    .filter(f -> !Files.isDirectory(f))
                    .filter(f -> f.getFileName().toString().endsWith(".xml"))
                    .map(f -> new SourceAndStream(f.toString(), 
                        FunctionUtils.runtimizing(() -> Files.newInputStream(f))))
                    .collect(toList());
                
                if (langDirFileStreams.isEmpty()) {
                    SelectMarkerBlockPlugin.logWarning("Language dir contians no XML files: " + langDir.get());
                }
            }
        }

        Unmarshaller unmarshaller = FunctionUtils.runtimize(() -> JAXBContext.newInstance(
                Languages.class.getPackageName(), SelectMarkerBlockPlugin.class.getClassLoader()).createUnmarshaller());
        unmarshaller.setListener(new JaxbPostConstructRunner());

        // Loop over both sources and parser them
        return Stream.concat(pluginContributionFileStreams, langDirFileStreams.stream())
            .flatMap(sourceAndStream -> {
                    try (var xmlStream = sourceAndStream.in.get()) {
                        return ((Languages) unmarshaller.unmarshal(xmlStream))
                            .languages.stream();
                    } catch (Exception exc) {
                        SelectMarkerBlockPlugin.logWarning("Failed to parse language " + sourceAndStream.source + " Message: " + exc);
                        return Stream.empty();
                    }
                })
            .collect(toList());
    }

    // Reads languages from plugin configuration file
    private void configLanguages(Collection<Language> languages) {
        suffixLanguageMap = languages.stream()
            .flatMap(l -> l.getExtensions().stream().map(s -> new SimpleEntry<>(s, l)))
            .collect(toLinkedHashMap());

        contentTypeLanguageMap = languages.stream()
            .flatMap(l -> l.getContentTypes().stream()
                .map(t -> Platform.getContentTypeManager().getContentType(t))
                .filter(Objects::isNull)
                .map(t -> new SimpleEntry<>(t, l)))
            .collect(toLinkedHashMap());
        
        Map<String, Language> nameLanguageMap = languages.stream()
            .collect(toLinkedHashMap(Language::getId));
        
        List<Language> defaults = languages.stream()
            .filter(l -> l.getExtensions().isEmpty())
            .collect(toList());
        
        if (defaults.isEmpty()) {
            defaultLanguage = Optional.empty();
        } else if (defaults.size() == 1) {
            defaultLanguage = Optional.of(defaults.get(0));
            nameLanguageMap.put(defaultLanguage.get().getId(), defaultLanguage.get());
        } else {
            throw new IllegalStateException("Config file contains multiple langauges with empty sufix list. Only one is allowed. Languages:"
                + defaults.stream().map(Language::getId).collect(joining(", ")));
        }
        
        languages.forEach(language -> language.setAllLanguages(nameLanguageMap));
        
        hasBeenInitialised = true;
    }
    
    @XmlRootElement
    public static class Languages {
        @XmlElement(name = "language")
        List<Language> languages = new ArrayList<>();
    }
    
    private static class SourceAndStream {
        String source;
        Supplier<InputStream> in;

        public SourceAndStream(String source, Supplier<InputStream> in) {
            this.source = source;
            this.in = in;
        }
    }
    

}




