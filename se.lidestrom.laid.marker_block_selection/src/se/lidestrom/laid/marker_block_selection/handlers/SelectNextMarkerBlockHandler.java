 package se.lidestrom.laid.marker_block_selection.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;

import se.lidestrom.laid.marker_block_selection.SelectBlockHandlerCommon;
import se.lidestrom.laid.marker_block_selection.internal.SelectMarkerBlock;

public class SelectNextMarkerBlockHandler extends AbstractHandler {
    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {
        SelectBlockHandlerCommon.runCommand(SelectMarkerBlock::selectNext);
        return null;
    }
}
