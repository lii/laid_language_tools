package se.lidestrom.laid.marker_block_selection.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.text.ITextSelection;

import se.lidestrom.editor_util.EditorHelper;
import se.lidestrom.laid.marker_block_selection.SelectBlockHandlerCommon;
import se.lidestrom.laid.marker_block_selection.SelectBlockHandlerCommon.EditorData;

public class RestoreSelectMarkerBlockHandler extends AbstractHandler {
    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {
        EditorHelper helper = EditorHelper.create(event);
        
        if (!helper.isTextEditor()) {
            // TODO: Log
            return null;
        }

        EditorData editorData = SelectBlockHandlerCommon.getEditorData(helper.getEditor());
        ITextSelection selection = editorData.selectionStack.pollFirst();
        
        if (selection != null) {
            SelectBlockHandlerCommon.doWithoutListener(helper.getEditor(), editorData.selectionChangedListener,
                () -> helper.getEditor().selectAndReveal(selection.getOffset(), selection.getLength()));
        }
        
        return null;
    }
}