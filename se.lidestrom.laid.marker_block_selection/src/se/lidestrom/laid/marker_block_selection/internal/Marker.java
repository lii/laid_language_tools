package se.lidestrom.laid.marker_block_selection.internal;

import static java.util.stream.Collectors.joining;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import se.lidestrom.editor_util.PatternAdapter;

/**
 * A marker represents a text sequence which can be found, such as the pair of '(' and ')'.
 */
@XmlAccessorType(XmlAccessType.FIELD)
final public class Marker {
    public enum Type {
        /** A marker which consists of one start and one end marker. */
        START_END, 
        /** 
         * A marker which consists of one big match for the whole region. 
         * Cannot contain other matches. 
         */
        REGION,
    }

    // Start marker pattern. Null if this is a Type.REGION.
    @XmlAttribute
    @XmlJavaTypeAdapter(PatternAdapter.class)
    private final Pattern start;

    // Start marker pattern. Null if this is a Type.REGION.
    @XmlAttribute
    @XmlJavaTypeAdapter(PatternAdapter.class)
    private final Pattern end;

    @XmlTransient
    private Marker.Type type;
    
    // Matches both start and end markers. Only this is used if marker is Type.REGION.
    @XmlTransient
    private Pattern both;

    // The pattern for Type.REGION markers. Null with this is Type.START_END.
    @XmlAttribute
    @XmlJavaTypeAdapter(PatternAdapter.class)
    private final Pattern region;
    
    @XmlAttribute
    private final String id;
    
    @PostConstruct
    private void postConstruct() {
        if (region == null) {
            if (start != null && end != null) {
                type = Type.START_END;
                both = Pattern.compile("(" + start.pattern() + "|" + end.pattern() + ")");
            } else {
                throw new IllegalStateException();
            }
        } else {
            if (start == null && end == null) {
                type = Type.REGION;
            both = region;
            } else {
                throw new IllegalStateException();
            }
        }
    }

    public Marker(String name, String region) {
        this(name, Type.REGION, null, null, Pattern.compile(region));
    }

    @SuppressWarnings("unused")
    private Marker() {
        this(null, null, null, null, null);
    }
    
    public Marker(String id, Marker.Type type, Pattern start, Pattern end, Pattern region) {
        this.id = id;
        this.type = type;
        this.start = start;
        this.end = end;
        this.region = region;
        
        if (id != null) postConstruct();
    }
    
    public Marker(String name, String start, String end) {
        this(name, Type.START_END, Pattern.compile(start), Pattern.compile(end), null);
    }
    
    @Override
    public int hashCode() {
        return getBoth().pattern().hashCode();
    }
    
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Marker)) return false; 
        return Objects.equals(getBoth().pattern(), ((Marker) obj).getBoth().pattern());
    }
    
    public List<String> getParts() {
        switch (getType()) {
            case REGION:
                return Arrays.asList(getBoth().pattern());
            case START_END:
                return Arrays.asList(getStart().pattern(), getEnd().pattern());
        }
        
        throw new IllegalStateException();
    }
    
    public boolean isEmpty() {
        switch (getType()) {
            case REGION:
                return getBoth().pattern().isEmpty();
            case START_END:
                return getStart().pattern().isEmpty() && getEnd().pattern().isEmpty();
        }
        
        throw new IllegalStateException();
    }
    
    @Override
    public String toString() {
        return "Marker< " + id + ">";
    }
    
    public static Marker makeCombined(Collection<Marker> allMarkers) {
        return new Marker("combined", 
            allMarkers.stream().flatMap(m -> m.getParts().stream()).collect(joining(")|(", "(", ")")));
    }

    public Pattern getBoth() {
        return both;
    }

    public Pattern getEnd() {
        return end;
    }

    public Pattern getStart() {
        return start;
    }

    public Marker.Type getType() {
        return type;
    }

    public String getId() {
        return id;
    }
    
}
