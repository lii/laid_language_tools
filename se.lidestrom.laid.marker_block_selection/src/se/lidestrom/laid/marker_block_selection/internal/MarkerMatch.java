package se.lidestrom.laid.marker_block_selection.internal;

import java.util.Arrays;
import java.util.Objects;

import org.eclipse.jface.text.IRegion;

class MarkerMatch {
    enum MarkerDirection {
        START, END;
    }

    public final int offset;
    public final String matchText;
    public final Marker marker;
    public final MarkerMatch.MarkerDirection dir;

    public MarkerMatch(int offset, String matchText, Marker marker, MarkerMatch.MarkerDirection dir) {
        this.offset = offset;
        this.matchText = matchText;
        this.marker = marker;
        this.dir = dir;
    }
    
    public static boolean isSameKind(MarkerMatch m1, MarkerMatch m2) {
        return m1.marker.equals(m2.marker);
    }

    private Object[] membersAsArray() {
        return new Object[] {offset, matchText, marker, dir};
    }

    public int getLeftOffset() {
        return offset;
    }
    
    public int getMatchSize(MarkerMatch other) {
        if (this.offset < other.offset) return other.getRightOffset() - this.getLeftOffset();
        else return this.getRightOffset() - other.getLeftOffset();
    }
    
    public boolean isInside(IRegion selection) {
        return isInside(selection.getOffset(), selection.getLength()); 
    }

    /**
     * @return true if this match is totally enclosed within the given region.
     */
    public boolean isInside(int offset, int length) {
        return this.offset >= offset && getRightOffset() <= offset + length; 
    }
    
    public int getRightOffset() {
        return offset + matchText.length();
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(membersAsArray());
    }
    
    @Override
    public String toString() {
        return "MarkerMatch[" + matchText + "@" + offset + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof MarkerMatch)) return false;
        return Arrays.equals(membersAsArray(), ((MarkerMatch) obj).membersAsArray());
    }
    
}