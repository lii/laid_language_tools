package se.lidestrom.laid.marker_block_selection.internal;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;

/**
 * A chunk of text. The algorithm examines one chunk at a time.
 */
class TextChunk implements CharSequence {
    final int offset;
    private final int length;
    private final IDocument doc;
    
    public TextChunk(int offset, int length, IDocument doc) {
        this.offset = offset;
        this.length = length;
        this.doc = doc;
    }

    public TextChunk(IRegion r, IDocument doc) {
        this(r.getOffset(), r.getLength(), doc);
    }
    
    @Override
    public int length() {
        return length;
    }

    @Override
    public char charAt(int index) {
        if (index > length) throw new IndexOutOfBoundsException();
        try {
            return doc.getChar(index + offset);
        } catch (BadLocationException exc) {
            throw new RuntimeException(exc);
        }
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        if (start < 0) throw new IndexOutOfBoundsException();
        if (end > length) throw new IndexOutOfBoundsException();
        if (start == 0 && end == length) return this;
        return new TextChunk(offset + start, end - start, doc);
    }
    
    @Override
    public String toString() {
        try {
            return doc.get(offset, length);
        } catch (BadLocationException exc) {
            return "TextChunkBad[ location: " + offset + ", " + length + "]";
        }
    }
}