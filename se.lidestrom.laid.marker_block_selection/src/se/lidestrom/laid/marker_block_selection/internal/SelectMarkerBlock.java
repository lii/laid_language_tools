package se.lidestrom.laid.marker_block_selection.internal;

import static se.lidestrom.editor_util.EditorUtils.runtimize;
import static se.lidestrom.editor_util.EditorUtils.streamFor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Spliterator;
import java.util.regex.Matcher;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.Region;

import se.lidestrom.editor_util.Box;
import se.lidestrom.laid.marker_block_selection.internal.MarkerMatch.MarkerDirection;

/**
 * Implementation of the actual marker selection algorithm.
 */
public class SelectMarkerBlock {
    
    public static Optional<IRegion> selectNext(
        IDocument doc, IRegion selection, 
        Marker combinedMarker, Collection<Marker> markers) throws BadLocationException {

        int firstLineNr = doc.getLineOfOffset(selection.getOffset() + selection.getLength());

        // Forwards
        Stream<TextChunk> chunksForwards = chunksForwards(doc, firstLineNr);
        Spliterator<MarkerMatch> matchesForwards = scanForMarkers(combinedMarker, markers, chunksForwards, false)
            .filter(m -> selection.getOffset() + selection.getLength() <= m.getLeftOffset())
            .spliterator();
        
        Box<MarkerMatch>  match = new Box<>(null);
        
        if (matchesForwards.tryAdvance(match)) {
            if (match.value.dir == MarkerDirection.START) {
                Stream<MarkerMatch> unmatchedForwards = findUnmachedMarkers(
                    MarkerDirection.END, StreamSupport.stream(matchesForwards, false));
                return unmatchedForwards.findFirst().map(endMatch -> 
                    new Region(selection.getOffset(), endMatch.getRightOffset() - selection.getOffset()));
            } else {
                return selectEnclosing(doc, selection, combinedMarker, markers);
            }
        } else {
            return Optional.empty();
        }
    }    
    
    
    public static Optional<IRegion> selectPrevious(
        IDocument doc, IRegion selection, 
        Marker combinedMarker, Collection<Marker> markers) throws BadLocationException {

        int firstLineNr = doc.getLineOfOffset(selection.getOffset());

        // Forwards
        Stream<TextChunk> chunksBackwards = chunksBackwards(doc, firstLineNr);
        Spliterator<MarkerMatch> matchesForwards = scanForMarkers(combinedMarker, markers, chunksBackwards, true)
            .filter(m -> !m.isInside(selection.getOffset(), doc.getLength() - selection.getOffset()))            
            .spliterator();
        
        Box<MarkerMatch> match = new Box<>(null);
        
        if (matchesForwards.tryAdvance(match)) {
            if (match.value.dir == MarkerDirection.END) {
                Stream<MarkerMatch> unmatchedForwards = findUnmachedMarkers(
                    MarkerDirection.START, StreamSupport.stream(matchesForwards, false));
                return unmatchedForwards.findFirst().map(startMatch -> 
                    new Region(startMatch.getLeftOffset(), selection.getOffset() - startMatch.getLeftOffset() + selection.getLength()));
            } else {
                return selectEnclosing(doc, selection, combinedMarker, markers);
            }
        } else {
            return Optional.empty();
        }
    }        
    
    
    public static Optional<IRegion> selectEnclosing(
        IDocument doc, IRegion selection, 
        Marker combinedMarker, Collection<Marker> markers) throws BadLocationException {
        int firstLineNr = doc.getLineOfOffset(selection.getOffset());

        // Forwards
        Stream<TextChunk> chunksForwards = chunksForwards(doc, firstLineNr);
        Stream<MarkerMatch> matchesForwards = scanForMarkers(combinedMarker, markers, chunksForwards, false)
            .filter(m -> !m.isInside(0, selection.getOffset()));
        Stream<MarkerMatch> unmatchedForwards = findUnmachedMarkers(MarkerDirection.END, matchesForwards);
        
        // Backwards
        Stream<TextChunk> chunksBackwards = chunksBackwards(doc, firstLineNr);
        Stream<MarkerMatch> matchesBackwards = scanForMarkers(combinedMarker, markers, chunksBackwards, true)
            .filter(m -> !m.isInside(selection.getOffset(), doc.getLength() - selection.getOffset()));
        Stream<MarkerMatch> unmatchedBackwards = findUnmachedMarkers(MarkerDirection.START, matchesBackwards);
        
        // The big final: Find the closest enclosing markers
        return findClosestMatchingMarkers(selection, unmatchedForwards.iterator(), unmatchedBackwards.iterator());
    }

    private static Stream<TextChunk> chunksBackwards(IDocument doc, int firstLineNr) {
        Stream<TextChunk> chunksBackwards = streamFor(
            firstLineNr,
            lineNr -> lineNr >= 0,
            lineNr -> lineNr - 1,
            lineNr -> new TextChunk(runtimize(() -> doc.getLineInformation(lineNr)), doc));
        return chunksBackwards;
    }

    private static Stream<TextChunk> chunksForwards(IDocument doc, int firstLineNr) {
        Stream<TextChunk> chunksForwards = streamFor(
            firstLineNr,
            lineNr -> lineNr < doc.getNumberOfLines(),
            lineNr -> lineNr + 1,
            lineNr -> new TextChunk(runtimize(() -> doc.getLineInformation(lineNr)), doc));
        return chunksForwards;
    }

    private static Stream<MarkerMatch> scanForMarkers(
        Marker combinedMarker, 
        Collection<Marker> specificMarkers, 
        Stream<TextChunk> chunks,
        boolean reverse) {
        // Scan lines and produce a stream of MarkMatches by matching the mark patterns
        // against the lines
        return chunks.flatMap((TextChunk chunk) -> {
            Matcher matcher = combinedMarker.getBoth().matcher(chunk);
            List<MarkerMatch> matches = new ArrayList<>();
            while (matcher.find()) {
                // Find specific matcher
                Marker specificMarker = specificMarkers.stream()
                    .filter(m -> m.getBoth().matcher(matcher.group()).find())
                    .findAny().get(); // This should always succeed
    
                switch (specificMarker.getType()) {
                    case START_END:
                        // Start-end markers get one match for each found marker
                        MarkerDirection dir = specificMarker.getStart().matcher(matcher.group()).find()
                            ? MarkerDirection.START : MarkerDirection.END;
                        matches.add(new MarkerMatch(chunk.offset + matcher.start(), matcher.group(), specificMarker, dir));
                        break;
                    case REGION:
                        // Region markers gets one match in the start of the marker and one match in the end
                        matches.add(new MarkerMatch(chunk.offset + matcher.start(), matcher.group().substring(0, 1), specificMarker, MarkerDirection.START));
                        matches.add(new MarkerMatch(chunk.offset + matcher.end() - 1, 
                            matcher.group().substring(matcher.group().length() - 1, matcher.group().length()), 
                            specificMarker, MarkerDirection.END));
                        break;
                    default:
                        throw new IllegalStateException();
                }
            }
    
            // Reverse matches order when we are going backwards
            if (reverse) Collections.reverse(matches);
    
            return matches.stream();
        });
    }

    private static Stream<MarkerMatch> findUnmachedMarkers(MarkerDirection soughtMarkerDir, Stream<MarkerMatch> markMatches) {
        Map<Marker, Integer> oppositeMarkerkMap = new HashMap<>();
        return markMatches.filter((MarkerMatch match) -> {
                int nrOppositeMarks = oppositeMarkerkMap.getOrDefault(match.marker, 0);
                
                if (match.dir == soughtMarkerDir && nrOppositeMarks == 0) {
                    return true;
                } else {
                    if (match.dir == soughtMarkerDir) oppositeMarkerkMap.put(match.marker, nrOppositeMarks - 1);
                    else oppositeMarkerkMap.put(match.marker, nrOppositeMarks + 1);
                    return false;
                }
            });
    }
    
    
    private static Optional<IRegion> findClosestMatchingMarkers(
        IRegion selection, 
        Iterator<MarkerMatch> iterForwards, 
        Iterator<MarkerMatch> iterBackwards)
    {
        // This class contains data that are used when searching forwards and backwards.
        // Two objects of this class are used, one for each direction.
        // The loop searches for the smallest match region (the region between start and end
        // mark). It alternates searching forwards and backwards, using the distance between
        // first start match and the last end match as the "match distance" to decide which
        // way to search.
        // 
        // The algorithm stops when it has examined markers on each side far enough so it's
        // certain there can be no shorter match.
        // 
        // This search handles the case where there are unmatched markers in the input, that is
        // the reason to why it is this complex.

        // There are no match if there to begin with are no markers on one of the sides
        if (!iterForwards.hasNext() || !iterBackwards.hasNext()) {
            return Optional.empty(); 
        }

        // Setup initial iteration state
        BestMatch bestMatch = new BestMatch();
        DirData forwardsData = new DirData(selection, iterForwards, iterForwards.next());
        DirData backwardsData = new DirData(selection, iterBackwards, iterBackwards.next());

        forwardsData.matchDist = backwardsData.matchDist
            = forwardsData.currentMatch.getMatchSize(backwardsData.currentMatch);

        forwardsData.examineNextMarker(backwardsData, bestMatch);
        backwardsData.examineNextMarker(forwardsData, bestMatch);
        
        while (true) {
            // If this is true there can be to smaller match region than the saved one
            if (forwardsData.matchDist >= bestMatch.matchDist && backwardsData.matchDist >= bestMatch.matchDist) {
                break;
            }
            
            // Go to the side with the smallest match dist
            if (forwardsData.matchDist < backwardsData.matchDist) {
                forwardsData.proceedAndExamineNextMarker(backwardsData, bestMatch);
            } else {
                backwardsData.proceedAndExamineNextMarker(forwardsData, bestMatch);
            }
        }
        
        // Algorithm has stopped. Return best match if there is one.
        if (bestMatch.marker != null) {
            MarkerMatch start = backwardsData.matches.get(bestMatch.marker);
            MarkerMatch end = forwardsData.matches.get(bestMatch.marker);
            return Optional.of(new Region(start.getLeftOffset(), end.getMatchSize(start)));
        } else {
            return Optional.empty();
        }
    }
    
    /**
     * Contains data that is specific for one search direction in the matching markers
     * search algorithm.
     */
    private static final class DirData {
        int matchDist = Integer.MIN_VALUE;
        // The latest found marker in this direction
        MarkerMatch currentMatch = null;
        // The first found marker in this direction
        final MarkerMatch firstMatch;
        // Maps marker to the first found marker of that type
        final Map<Marker, MarkerMatch> matches = new HashMap<>();

        final Iterator<MarkerMatch> iterator;
        final IRegion selection;
        
        DirData(IRegion selection, Iterator<MarkerMatch> iterator, MarkerMatch firstMatch) {
            this.iterator = iterator;
            this.selection = selection;
            this.currentMatch = firstMatch;
            this.firstMatch = firstMatch;
        }

        
        void proceedAndExamineNextMarker(DirData oppositeData, BestMatch bestMatch) {
            // Get next element from iterator if that has not been done already
            if (!iterator.hasNext()) {
                // No more elements is counted as match indefinitely far away 
                matchDist = Integer.MAX_VALUE;
            } else {
                currentMatch = iterator.next();
                examineNextMarker(oppositeData, bestMatch);
            }
        }
        
        void examineNextMarker(DirData oppositeData, BestMatch bestMatch) {
            matchDist = currentMatch.getMatchSize(oppositeData.firstMatch);

            if (!matches.containsKey(currentMatch.marker)) {
                // Save the first found marker of a certain kind
                matches.put(currentMatch.marker, currentMatch);

                // Check for matching markers of this kind
                MarkerMatch oppositeMarker = oppositeData.matches.get(currentMatch.marker);
                // If there is a match and no previous marker of this kind is found
                // (disregard matches inside the selection)...
                if (oppositeMarker != null && !currentMatch.isInside(selection)
                    && !oppositeMarker.isInside(selection)) {
                    int newMatchDist = currentMatch.getMatchSize(oppositeMarker);
                    // ...if this is the best (ie smallest) match region... 
                    if (newMatchDist < bestMatch.matchDist) {
                        // ...save info about the best match
                        bestMatch.marker = currentMatch.marker;
                        bestMatch.matchDist = newMatchDist;
                    }
                }
            }
        }
    }
    
    // Contains data about the best match for one start and one end marker
    private final static class BestMatch {
        int matchDist = Integer.MAX_VALUE;
        Marker marker = null;
    }
}
