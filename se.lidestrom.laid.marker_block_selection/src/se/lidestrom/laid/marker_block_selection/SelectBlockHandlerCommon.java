package se.lidestrom.laid.marker_block_selection;

import java.util.ArrayDeque;
import java.util.Collection;
import java.util.Deque;
import java.util.Optional;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.ui.texteditor.ITextEditor;

import se.lidestrom.editor_util.EditorHelper;
import se.lidestrom.laid.marker_block_selection.internal.Marker;

import static se.lidestrom.editor_util.EditorUtils.selectionToRegion;

/**
 * Code that is used by all select block command handlers. This is most of the code that
 * interacts with the Eclipse framework.
 * 
 * - Saves state for the editors.
 * - Common run-command method for all select handlers.
 * - Reference to global language container.
 */
public class SelectBlockHandlerCommon {
    private SelectBlockHandlerCommon() {} // Static class
    
    private static final String EDITOR_DATA_ID = SelectMarkerBlockPlugin.PLUGIN_ID + ".editorData";
    
    private static final LanguageContainer languageContainer = new LanguageContainer();
    
    public static class EditorData {
        public final Deque<ITextSelection> selectionStack = new ArrayDeque<>();
        public final ISelectionChangedListener selectionChangedListener = _e -> selectionStack.clear();
    }
    
    public static void doWithoutListener(ITextEditor editor, ISelectionChangedListener listener, Runnable action) {
        try {
            editor.getSelectionProvider().removeSelectionChangedListener(listener);
            action.run();
        } finally {
            editor.getSelectionProvider().addSelectionChangedListener(listener);
        }
    }
    
    
    public interface SelectBlockCommand {
        Optional<IRegion> performOperation(IDocument doc, IRegion selection, 
            Marker combinedMarker, Collection<Marker> markers) throws BadLocationException;
    }
    
    public static void runCommand(SelectBlockCommand command) {
        EditorHelper helper = EditorHelper.createForActiveEditor();
        
        if (!helper.isTextEditor()) {
            SelectMarkerBlockPlugin.logWarning("Select block command was invoked on an editor this is not a text editor");
            return;
        }
        
        Language language = languageContainer.getLanguageForEditor(helper.getEditor())
            .orElse(null);
        
        if (language == null) {
            SelectMarkerBlockPlugin.logWarning("Select block command was invoked on an document for which there is no language defined.");
            return;
        }
        
        try {
            IRegion newSelection = command.performOperation(
                helper.getDocument(), selectionToRegion(helper.getSelection()), 
                language.getCombinedMarker(), language.getMarkers())
                .orElse(null);
            
            if (newSelection != null) {
                EditorData editorData = getEditorData(helper.getEditor());
                ITextSelection selection = (ITextSelection) helper.getEditor().getSelectionProvider().getSelection();
                editorData.selectionStack.addFirst(selection);
                doWithoutListener(helper.getEditor(), editorData.selectionChangedListener,
                    () -> helper.getEditor().selectAndReveal(newSelection.getOffset(), newSelection.getLength()));
            }

        } catch (BadLocationException exc) {
            throw new RuntimeException(exc);
        }
    }
    
    public static void selectAndSaveOldSelection(IRegion newSelection, ITextEditor editor) {
        EditorData editorData = getEditorData(editor);
        ITextSelection selection = (ITextSelection) editor.getSelectionProvider().getSelection();
        editorData.selectionStack.addFirst(selection);

        doWithoutListener(editor, editorData.selectionChangedListener,
            () -> editor.selectAndReveal(newSelection.getOffset(), newSelection.getLength()));
    }

    public static EditorData getEditorData(ITextEditor editor) {
        return (EditorData) editor.getSite()
            .getService(MPart.class)
            .getTransientData()
            .computeIfAbsent(EDITOR_DATA_ID, _k -> new EditorData());
    }

}
