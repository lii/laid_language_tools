package se.lidestrom.laid.marker_block_selection;

import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class SelectMarkerBlockPlugin extends AbstractUIPlugin {
    
    // The plug-in ID
    public static final String PLUGIN_ID = "se.lidestrom.laid.marker_block_selection"; //$NON-NLS-1$
    public static final String LANGUAGE_DIR_VAR_NAME = SelectMarkerBlockPlugin.PLUGIN_ID + ".languageDir";

    public static final String LANGUAGES_EXTENTION_POINT_ID = SelectMarkerBlockPlugin.PLUGIN_ID + ".languages";

    // The shared instance
    private static SelectMarkerBlockPlugin plugin;
    
    /**
     * The constructor
     */
    public SelectMarkerBlockPlugin() {}

    @Override
    public void start(BundleContext context) throws Exception {
        super.start(context);
        plugin = this;
    }
    
    @Override
    public void stop(BundleContext context) throws Exception {
        plugin = null;
        super.stop(context);
    }
    
    /**
     * Returns the shared instance
     */
    public static SelectMarkerBlockPlugin getDefault() {
        return plugin;
    }
    
    /**
     * Returns an image descriptor for the image file at the given plug-in
     * relative path
     */
    public static ImageDescriptor getImageDescriptor(String path) {
        return imageDescriptorFromPlugin(PLUGIN_ID, path);
    }
    
    public static void logWarning(String msg) {
        log(Status.WARNING, msg);
    }
    
    public static void logError(String msg) {
        log(Status.ERROR, msg);
    }
    
    public static void log(int status, String msg) {
        getDefault().getLog().log(new Status(status, PLUGIN_ID, msg));
    }
}
