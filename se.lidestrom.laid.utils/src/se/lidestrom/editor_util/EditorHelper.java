package se.lidestrom.editor_util;

import java.util.Optional;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextOperationTarget;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.ITextViewerExtension5;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.texteditor.ITextEditor;

/**
 * A class which wraps a ITextEditor and helps the client to perform common tasks related
 * to that.
 */
public class EditorHelper {
    private final Optional<ITextEditor> editor;
    private final LazyValue<ITextViewer> viewer;
    private final LazyValue<IDocument> docValue;
    private final LazyValue<IHandlerService> handlerService;
    
    public EditorHelper(final Optional<ITextEditor> editor) {
        this.editor = editor;
        docValue = new LazyValue<>(() -> getEditor().getDocumentProvider().getDocument(getEditor().getEditorInput()));
        viewer = new LazyValue<>(() -> (ITextViewer) getEditor().getAdapter(ITextOperationTarget.class));
        handlerService = new LazyValue<>(() -> PlatformUI.getWorkbench().getService(IHandlerService.class));
    }
    
    public int widgetOffset2ModelOffset(int offset) {
        if (getViewer() instanceof ITextViewerExtension5) {
            return ((ITextViewerExtension5) getViewer()).widgetOffset2ModelOffset(offset);
        } else {
            return offset;
        }
    }
    
    public boolean isTextEditor() {
        return editor.isPresent();
    }
    
    public int getCaretOffset() {
        // This returns the wrong offset sometimes. Started with Kepler.
        // return JFaceTextUtil.getOffsetForCursorLocation( getViewer() );
        return widgetOffset2ModelOffset(getViewer().getTextWidget().getCaretOffset());
    }
    
    /**
     * Returns a new EditorHelper for the active part for the event. Returns empty() if
     * the part in not a ITextEditor.
     */
    public static EditorHelper create(ExecutionEvent event) {
        try {
            return new EditorHelper(EditorUtils.partToEditor(HandlerUtil.getActivePartChecked(event)));
        } catch (ExecutionException exc) {
            throw new RuntimeException(exc);
        }
    }
    
    /**
     * Returns a new EditorHelper for the active part. Returns empty() if
     * the part in not a ITextEditor.
     */
    public static EditorHelper createForActiveEditor() {
        return createFromPart(PlatformUI.getWorkbench()
            .getActiveWorkbenchWindow().getActivePage().getActiveEditor());
    }
    
    public Optional<IFile> getEditorFile() {
        return EditorUtils.getEditorFile(getEditor());
    }

    public static EditorHelper createFromPart(IWorkbenchPart part) {
        return new EditorHelper(EditorUtils.partToEditor(part));
    }
    
    public IHandlerService getHandlerService() {
        return handlerService.get();
    }
    
    
    public ITextEditor getEditor() {
        return editor.get();
    }
    
    public ITextSelection getSelection() {
        return (ITextSelection) getEditor().getSelectionProvider().getSelection();
    }
    
    /**
     * Modified version of getSelection. Returns selection in a format that
     * takes the orientation of the selection into account.
     * 
     * It is on this format: Offset is for the end of selection where caret
     * isn't located. Length is difference of offset from start of selection to
     * caret, positive or negative.
     * 
     * In this format selectAndReveal(getModSelection()) does not modify
     * selection of caret, regardless of selection orientation.
     */
    public ITextSelection getSelectionModified() {
        ITextSelection sel = getSelection();
        int carOffs = getCaretOffset();
        int modLength = carOffs == sel.getOffset() ? -sel.getLength() : sel.getLength();
        
        return new TextSelection(carOffs - modLength, modLength);
    }
    
    public IDocument getDocument() {
        return docValue.get();
    }
    
    public ITextViewer getViewer() {
        return viewer.get();
    }
    
    public ITextViewerExtension5 getViewerExt5() {
        return (ITextViewerExtension5) getViewer();
    }
    
    public ISelectionProvider getSelectionProvider() {
        return getEditor().getSelectionProvider();
    }
    
    public void printSelection() {
        ITextSelection sel = getSelectionModified();
        System.out.println("Selection offset: " + sel.getOffset() + " length: " + sel.getLength());
    }
    
}