package se.lidestrom.editor_util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.Collector;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.dialogs.IPageChangeProvider;
import org.eclipse.jface.text.ITextOperationTarget;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.ITextViewerExtension5;
import org.eclipse.jface.text.Region;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IPathEditorInput;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.ITextEditor;

/**
 * Misc utilities for the editor util plugin.
 */
public class EditorUtils {

    
    private EditorUtils() {}
    
    public static Region selectionToRegion(ITextSelection s) {
        return new Region(s.getOffset(), s.getLength());
    }

    public static ITextSelection regionToselection(ITextSelection r) {
        return new TextSelection(r.getOffset(), r.getLength());
    }
    
    public static Map<IFile, List<IEditorInput>> getOpenEditors() {
        Map<IFile, List<IEditorInput>> inputs = new LinkedHashMap<>();
            
        Stream.of(PlatformUI.getWorkbench().getWorkbenchWindows())
            .map(w -> w.getActivePage())
            .flatMap(p -> Stream.of(p.getEditorReferences()))
            .map(FunctionUtils.runtimizing((IEditorReference r) ->  r.getEditorInput()))
            .forEach(i -> inputs.computeIfAbsent(
                EditorUtils.getEditorInputFile(i).orElse(null), 
                _f -> new ArrayList<>())
                    .add(i));
        
        inputs.remove(null);
        
        return inputs;
    }
    
    public static Optional<ITextEditor> partToEditor(IWorkbenchPart part) {
        Object editor = part;
        
        if (editor instanceof IPageChangeProvider) {
            editor = ((IPageChangeProvider) editor).getSelectedPage();
        }
         
        if (editor instanceof ITextEditor) return Optional.of((ITextEditor) editor);
        else return Optional.empty(); 
    }

    public static Optional<String> getEditorResource(IEditorPart editor) {
        IEditorInput input = editor.getEditorInput();
        if (input instanceof IPathEditorInput) return Optional.of(((IPathEditorInput) input).getPath().toString());
        return getEditorInputFile(input).map(f -> f.toString());
    }

    
    public static Optional<IFile> getEditorInputFile(IEditorInput input) {
        if (input instanceof IFileEditorInput) return Optional.of(((IFileEditorInput) input).getFile());
        else return Optional.empty();
    }
    
    public static Optional<IFile> getEditorFile(IEditorPart editor) {
        return getEditorInputFile(editor.getEditorInput());
    }
    
    public static String fileExtension(String fileName) {
        int dotIx = fileName.lastIndexOf('.');
        
        if (dotIx != -1 && dotIx < fileName.length() - 1) {
            return fileName.substring(dotIx);
        } else {
            return "";
        }
    }
    
    public static <T> Stream<T> readStream(Stream<T> s) {
//        List<T> l = s.collect(toList());
//        return l.stream();
        return s;
    }
    
    /**
     * A for-loop line generator for streams.
     */
    public static <T, S> Stream<T> streamFor(S initial,
        Predicate<? super S> tester,
        UnaryOperator<S> updater,
        Function<? super S, ? extends T> producer) {
        
        AtomicReference<S> state = new AtomicReference<>(initial);
        return StreamSupport.stream(
            new Spliterators.AbstractSpliterator<T>(Long.MAX_VALUE, Spliterator.SIZED) {
                @Override
                 public boolean tryAdvance(Consumer<? super T> action) {
                     S s = state.get();
                    boolean goOn = tester.test(s);
                     if (goOn) {
                         action.accept(producer.apply(s));
                         state.updateAndGet(updater);
                     }
                     return goOn;
                 }
            }, false);
    }

    
    public static int getCaretOffset(ITextEditor editor) {
        ITextViewerExtension5 viewer = (ITextViewerExtension5) editor.getAdapter(ITextOperationTarget.class);
        return viewer.widgetOffset2ModelOffset(((ITextViewer) viewer).getTextWidget().getCaretOffset());
    }
    
    public static <T> T runtimize(Callable<T> a) {
        try {
            return a.call();
        } catch (RuntimeException exc) {
            throw exc;
        } catch (Exception exc) {
            throw new RuntimeException("Wrapped in runtimize", exc);
        }
    }
    
    public static <T> Stream<T> generateFinite(Supplier<? extends Optional<? extends T>> s) {
        return StreamSupport.stream(
            new Spliterators.AbstractSpliterator<T>(Long.MAX_VALUE, Spliterator.SIZED) {
                @Override
                 public boolean tryAdvance(Consumer<? super T> action) {
                     Optional<? extends T> v = s.get();
                     if (v.isPresent()) action.accept(v.get());
                     return v.isPresent();
                 }
            }, false);
    }

    
    public static <T, T2 extends T> Iterable<T> toIterable(Supplier<? extends Optional<T2>> s) {
        return () ->
            new Iterator<T>() {
                private Optional<T2> nextElem = null;
                @Override
                public boolean hasNext() {
                    if (nextElem != null) return true;
                    nextElem = s.get();
                    return nextElem.isPresent();
                }
                @Override
                public T next() {
                    if (!hasNext()) throw new NoSuchElementException();
                    T2 v = nextElem.get();
                    nextElem = null;
                    return v;
                }
        };
    }
    
    
    public static Optional<IProject> getSelectedProject(ISelection selection) {
        if (selection instanceof IStructuredSelection) {
            for (Iterator<?> it = ((IStructuredSelection) selection).iterator(); it.hasNext();) {
                Object element = it.next();
                if (element instanceof IProject) {
                    return Optional.of((IProject) element);
                } else if (element instanceof IAdaptable) {
                    return Optional.of((IProject) ((IAdaptable) element).getAdapter(IProject.class));
                }
            }
        }

        return Optional.empty();
    }
    
    public static <E, K> Collector<E, ?, LinkedHashMap<K, E>> toLinkedHashMap(Function<E, K> keyExtractor) {
        return toLinkedHashMap(keyExtractor, x -> x);
    }
    
    public static <E, K, V> Collector<E, ?, LinkedHashMap<K, V>> toLinkedHashMap(Function<E, K> keyExtractor, Function<E, V> valueExtractor) {
        return Collector.of(
            LinkedHashMap::new,
            (m, e) -> m.put(keyExtractor.apply(e), valueExtractor.apply(e)),
            (m1, m2) -> { m1.putAll(m2); return m1; });
    }

    public static <E, K, V> Collector<Entry<K, V>, ?, LinkedHashMap<K, V>> toLinkedHashMap() {
        return toLinkedHashMap(Entry::getKey, Entry::getValue);
    }
    
    
    @SuppressWarnings("unchecked")
    public static <K, V> LinkedHashMap<K, V> newLinkedHashMap(Object... keyValList) {
        LinkedHashMap<K, V> map = new LinkedHashMap<>();
        
        for (int i = 0; i < keyValList.length; i += 2) {
            map.put((K) keyValList[i], (V) keyValList[i + 1]);
        }
        
        return map;
    }
}
