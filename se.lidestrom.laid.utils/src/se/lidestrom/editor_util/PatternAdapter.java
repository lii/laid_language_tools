package se.lidestrom.editor_util;

import java.util.regex.Pattern;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Converts a regex Pattern to and from a normal string.
 */
public class PatternAdapter extends XmlAdapter<String, Pattern> {

    @Override
    public Pattern unmarshal(String v) throws Exception {
        return v == null ? null : Pattern.compile(v);
    }

    @Override
    public String marshal(Pattern v) throws Exception {
        return v == null ? null : v.pattern();
    }
}