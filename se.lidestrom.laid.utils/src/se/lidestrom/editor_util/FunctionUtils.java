package se.lidestrom.editor_util;

import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Thing that makes it easier to work with lambdas.
 */
public class FunctionUtils {
    public interface ThrowingFunction<A, R> {
        public R apply(A a) throws Exception;
    }

    public interface ThrowingPredicate<A> {
        public boolean test(A a) throws Exception;
    }

    public static <A, R> Function<A, R> runtimizing(ThrowingFunction<A, R> f) {
        return a -> {
            try {
                return f.apply(a);
            } catch (RuntimeException exc) {
                throw exc;
            } catch (Exception exc) {
                throw new RuntimeException("Wrapped in runtimize", exc);
            }
        };
    }
    
    public static <A> Predicate<A> runtimize(ThrowingPredicate<A> f) {
        return a -> {
            try {
                return f.test(a);
            } catch (RuntimeException exc) {
                throw exc;
            } catch (Exception exc) {
                throw new RuntimeException("Wrapped in runtimize", exc);
            }
        };
    }

    public static interface SpecificThrowingConsumer<T, E extends Throwable> {
        void accept(T o) throws E;
    }

    public static interface SpecificThrowingFunction<A, R, E extends Throwable> {
        R apply(A o) throws E;
    }
    
    public static interface ThrowingConsumer<T> {
        void accept(T o) throws Exception;
    }
    
    public static <T> T runtimize(Callable<T> a) {
        try {
            return a.call();
        } catch (RuntimeException exc) {
            throw exc;
        } catch (Exception exc) {
            throw new RuntimeException("Wrapped in runtimize", exc);
        }
    }

    public static <T> Supplier<T> runtimizing(Callable<T> a) {
        return () -> {
            try {
                return a.call();
            } catch (RuntimeException exc) {
                throw exc;
            } catch (Exception exc) {
                throw new RuntimeException("Wrapped in runtimizing", exc);
            }
        };
    }
    
}
