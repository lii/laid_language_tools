package se.lidestrom.editor_util;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class EntryNameAdapter<K, V, E extends EntryNameAdapter.NamedEntry> extends XmlAdapter<Map<K, V>, List<E>> {

    private final Class<? extends E> entryClass;
    
    public interface NamedEntry {
        
        Object getKey();
        
        void setKey(Object key);
        
        Object getValue();
        
        void setValue(Object value);
        
    }    
    
    public static abstract class SimpleNamedEntry implements NamedEntry  {
        public Object key;
        public Object value;

        @Override
        public Object getKey() {
            return key;
        }
        @Override
        public void setKey(Object key) {
            this.key = key;
        }
        @Override
        public Object getValue() {
            return value;
        }
        @Override
        public void setValue(Object value) {
            this.value = value;
        }
    }
    
    @SuppressWarnings("unchecked")
    public EntryNameAdapter() {
        entryClass = (Class<E>) ((ParameterizedType) this.getClass().getGenericSuperclass())
            .getActualTypeArguments()[0];
    }

    @Override
    public List<E> unmarshal(Map<K, V> v) throws Exception {
        List<E> l = new ArrayList<>(); 
        for (Map.Entry<K, V> e : v.entrySet()) {
            E n = entryClass.getConstructor().newInstance();
            n.setKey(e.getKey());
            n.setValue(e.getValue());
            l.add(n);
        }
        return l;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Map<K, V> marshal(List<E> v) throws Exception {
        Map<K, V> m = new LinkedHashMap<>();
        for (E n : v) {
            m.put((K) n.getKey(), (V) n.getValue());
        }
        return m;
    }
    
    
    
    
    
}
