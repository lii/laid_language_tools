package se.lidestrom.editor_util;

import static java.util.Collections.emptyList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import se.lidestrom.editor_util.special.ResultGeneric;



/**
 * Utility class to be used as return value from method that can fail. Contains either a 
 * result object or a non-empty list of error messages.
 * 
 * @param <T> The type of the result value
 */
public class Result<T> extends ResultGeneric<T, String, Result<T>> {
    @Override
    protected Result<T> newSubclass(T result, List<String> errorMessage) {
        return new Result<>(result, errorMessage);
    }

    @Override
    protected Result<T> thisAsSubclass() {
        return this;
    }
    private static final String DEFAULT_MESSAGE_SEPARATOR = "; ";
    
    public Result(T result, List<String> errorMessage) {
        super(result, errorMessage);
    }

    public Result(T result, String errorMessage) {
        super(result, errorMessage);
    }
    
    public String getErrorMessage(String messageSeparator) {
        if (isSuccess()) throw new IllegalStateException();
        return String.join(messageSeparator, getErrorMessages());
    }

    public String getErrorMessage() {
        return getErrorMessage(DEFAULT_MESSAGE_SEPARATOR);
    }
    
    public static <T> Result<T> failure(String errorMessage) {
        return new Result<>(null, errorMessage);
    }
    
    public static <T> Result<T> success(T result) {
        return new Result<>(result, emptyList());
    }

    public static <T> Result<T> ofNullable(T o, String message) {
        return o == null ? failure(message) : success(o);
    }

    public static <T> Result<T> ofOptional(Optional<T> o, String errorMessage) {
        return o.isPresent() ? Result.success(o.get()) : Result.failure(errorMessage);
    }
    
    public static <A, R, E extends Throwable> 
    Function<A, Result<R>> failureOnException(Function<A, ? extends R> op, Class<E> excClass, Function<? super E, String> excHandler) {
        return failureOnExceptionGeneric(op, excClass, excHandler, Result::success, Result::failure);
    }

    public <NEW_T> Result<NEW_T> continueIfSuccess(Function<? super T, Result<NEW_T>> nextOperation) {
        return continueIfSuccessGeneric(nextOperation);
    }
    
    public <NEW_T> Result<NEW_T> fromNullableIfSuccess(Function<T, Result<NEW_T>> nextOperation, String message) {
        return fromNullableIfSuccessGeneric(nextOperation, message);
    }
    
    public <NEW_T> Result<NEW_T> map(Function<? super T, ? extends NEW_T> f) {
        return mapGeneric(f);
    }
}






