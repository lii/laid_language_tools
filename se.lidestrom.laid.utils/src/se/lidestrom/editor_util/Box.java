package se.lidestrom.editor_util;

import java.util.function.Consumer;

/**
 * A simple class that contains one mutable other value.
 * 
 * Useful when you have to use a method argument for an output value, such as when using
 * spliterators, or when your modifying a value in a lambda.
 */
public class Box<T> implements Consumer<T> {
    public T value;

    public Box(T value) {
        this.value = value;
    }

    public T get() {
        return value;
    }

    @Override
    public void accept(T value) {
        this.value = value;
    }
}
