package se.lidestrom.editor_util;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.xml.bind.Unmarshaller;

/**
 * An unmarshal listener which calls methods annotated with {@link PostConstruct} on
 * unmarshalled objects.
 */
public class JaxbPostConstructRunner extends Unmarshaller.Listener {
    @Override
    public void afterUnmarshal(Object object, Object arg1) {
        Class<?> type = object.getClass();
        List<Method> postConstructMethods = new ArrayList<>();
        
        do {
            int nrFound = 0;
            for (Method method : type.getDeclaredMethods()) {
                if (method.isAnnotationPresent(PostConstruct.class)) {
                    if (!Modifier.isFinal(method.getModifiers() | type.getModifiers())) {
                        throw new IllegalArgumentException("post construct method [" + method.getName() + "] must be final");
                    }
                    nrFound++;
                    postConstructMethods.add(method);
                }
                
                if (nrFound > 1) {
                    throw new IllegalStateException("@PostConstruct used multiple times on " + type.getName());
                }
            }
            
            type = type.getSuperclass();
        } while (type != null);
        
        for (Method m : postConstructMethods) {
            try {
                m.setAccessible(true);
                m.invoke(object);
            } catch (ReflectiveOperationException ex) {
                throw new RuntimeException(ex);
            }
        }
    }
    
}