package se.lidestrom.editor_util;

import java.util.Optional;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * For using Optional for XML binding.
 */
public class OptionalAdapter<T> extends XmlAdapter<T, Optional<T>> {

    @Override
    public Optional<T> unmarshal(T v) throws Exception {
        return Optional.ofNullable(v);
    }

    @Override
    public T marshal(Optional<T> v) throws Exception {
        return v.orElse(null);
    }
}