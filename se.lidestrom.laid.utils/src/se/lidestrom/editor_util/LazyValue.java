package se.lidestrom.editor_util;

import java.util.Objects;
import java.util.function.Supplier;

/**
 * A values which is lazily computed using an initialisation function on first read.
 * 
 * @param <T> The type of the value
 */
public class LazyValue<T> implements Supplier<T> {

    // values needn't be volatile since it's always accessed after creator
    private T value;
    private volatile Supplier<? extends T> creator;
    
    public LazyValue(Supplier<? extends T> creator) {
        this.creator = Objects.requireNonNull(creator);
    }
    
    @Override
    public T get() {
        // Double-check idiom for lazy initialization of instance fields. See Effective Java 
        // 2nd eddition, Item 71. This is addapted to work with two different fields. I hope
        // the changes are correct...
        Supplier<? extends T> c = creator;
        if (c != null) {
            synchronized (this) {
                c = creator;
                if (c != null) {
                    value = creator.get();
                    creator = null;
                }
            }
        }
        return value;
    }
}
