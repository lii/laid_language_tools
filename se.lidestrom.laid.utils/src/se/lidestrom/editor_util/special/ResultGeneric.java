package se.lidestrom.editor_util.special;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.Collections.unmodifiableList;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public abstract class ResultGeneric<T, E, THIS extends ResultGeneric<T, E, THIS>> {
    private final T result;
    // Note that a non-empty list is used to indicate success
    private final List<E> errorMessages;

    public ResultGeneric(T result, List<E> errorMessage) {
        this.result = result;
        this.errorMessages = Objects.requireNonNull(errorMessage);
    }

    public ResultGeneric(T result, E errorMessage) {
        this(result, singletonList(errorMessage));
    }
    
    public boolean isSuccess() {
        return errorMessages.isEmpty();
    }
    
    protected abstract THIS newSubclass(T result, List<E> errorMessage);
    
    protected abstract THIS thisAsSubclass();
    
    /**
     * If this {@code Result} is failure, then return a result with the same error messages, 
     * else return the result of applying {@code nextOperation} to the value of this. 
     * <p/>
     * This method correspond to monad bind. Also to flatMap.
     * <p/>
     * {@code bind :: Result<T> -> (T -> Result<R>) -> Result<T>}
     * <p/>
     * Warning: Not type-safe! Create non-generic version is subclass and call this one
     * for a type-safe alternative.
     */
    @SuppressWarnings("unchecked")
    protected <NEW_T, NEW_RESULT extends ResultGeneric<NEW_T, E, NEW_RESULT>>
    NEW_RESULT continueIfSuccessGeneric(Function<? super T, NEW_RESULT> nextOperation) {
        // This cast is safe because there is not result value of type T, only string messages
        return isSuccess() ? nextOperation.apply(get()) : (NEW_RESULT) this;
    }
    
    @SuppressWarnings("unchecked")
    protected <NEW_T, NEW_RESULT extends ResultGeneric<NEW_T, E, NEW_RESULT>>
    NEW_RESULT fromNullableIfSuccessGeneric(Function<T, NEW_RESULT> nextOperation, E message) {
        return continueIfSuccessGeneric(a -> {
              NEW_RESULT r = nextOperation.apply(a);
              return (NEW_RESULT) (r == null
                  ? newSubclass(null, singletonList(message))
                  : newSubclass((T) r, emptyList()));
        });
    }

    /*
     * Warning: Not type-safe! Create non-generic version is subclass and call this one
     * for a type-safe alternative.
     */
    @SuppressWarnings("unchecked")
    protected <NEW_T, NEW_RESULT extends ResultGeneric<NEW_T, E, NEW_RESULT>> 
    NEW_RESULT mapGeneric(Function<? super T, ? extends NEW_T> f) {
        if (isSuccess()) {
            return (NEW_RESULT) newSubclass((T) f.apply(get()), emptyList());
        } else {
            return (NEW_RESULT) this;
        }
    }
  

    protected static <A, R, ERR, E extends Throwable, RES extends ResultGeneric<R, ERR, RES>> 
    Function<A, RES> failureOnExceptionGeneric(
        Function<A, ? extends R> op, Class<E> excClass, 
        Function<? super E, ? extends ERR> excHandler, 
        Function<? super R, RES> success, 
        Function<? super ERR, RES> failure)
    {
        return a -> {
            try {
                return success.apply(op.apply(a));
            } catch (Throwable t) {
                if (excClass.isInstance(t)) {
                    return failure.apply(excHandler.apply(excClass.cast(t)));
                } else {
                    throw t;
                }
            }
        };
    }

    /**
     * If this {@code Result} is a success then return this, else return the result of
     * {@code nextOperation}. If that is a failure, merge its error messages with the messages
     * of this.
     * Type: Result<T> -> Result<T> -> Result<T>
     */
    public THIS tryIfFailure(Supplier<THIS> nextOperation) {
        if (isSuccess()) {
            return thisAsSubclass();
        } else {
            THIS nextResult = nextOperation.get();
            if (nextResult.isSuccess()) {
                return nextResult;
            } else {
                List<E> ms = new ArrayList<>(errorMessages);
                ms.addAll(nextResult.getErrorMessages());
                return newSubclass(null, unmodifiableList(ms));
            }
        }
    }    
    
    public T getOrDefault(T def) {
        return isSuccess() ? get() : def;
    }
    
    public List<E> getErrorMessages() {
        return errorMessages;
    }
    
    public <EXC extends Throwable> T getOrThrow(Function<List<E>, EXC> s) throws EXC {
        if (isSuccess()) return get();
        else throw s.apply(getErrorMessages());
    }
    
    
    public T get() {
        if (!isSuccess()) throw new IllegalStateException();
        return result;
    }
    
    public void doIfSuccess(Consumer<? super T> f) {
        if (isSuccess()) f.accept(get());
    }
}
