# Editor Utils Utils #

This project contains some utility code for working with the Eclipse framework. It was deemed to small to motivate having it as a separate plug-in. It is instead included in the actual plug-ins as source code. This is achieved by a workspace soft link from the plug-in directories to the source directory of the project.
