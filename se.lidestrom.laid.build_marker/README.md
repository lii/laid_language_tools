
# Build Problem Marker Plug-in

The Build Problem Marker is a plug-in for creating problem markers in editors and the Problems view from the output of Program Builders.

The plug-in attaches parsers on a program builders and listens to build output. It then sends this output to a custom parser class, then uses the result from the parser to create problem markers.

*Program builders* is a standard Eclipse feature. They call external programs during builds. This plug-in works by hooking into these builders and sending their output to custom parsers classes.

A program builder with a parser can be used to easily add an integrated builder for a new language which doesn't have an advanced plug-in. Together with the other tools in the Laid this suit this plug-in makes it easier to provide support for alternative languages in Eclipse.

A output parser could also be used to, for example, detect exception stack trace printouts and put error markers in the source file where the exception happened.

## Screenshots

![Screenshot][editor_view_screenshot]

![Configuration tab][config_tab_screenshot]

## Installation

See instructions located in the root directory of this repository.

## Description

The project consists of the following components that the user needs the deal with.

1. *Program Builders*: This is a standard Eclipse feature. They are created and edited using the normal Eclipse dialog for builders (*Project Properties* -> Builders -> *New...* -> *Program*). This plug-in adds the "Problem Markers" tag to this dialog where the path to a parser configuration file is set. They store their configuration data in `.launch` files, often in the `.externalBuilders` directory.
2. *Parser configuration files*: These are XML files where build output parsers are configured. For an explanation of this see the `res/parsers/ghc_parser.xml` example file. 
3. *Output parser classes*: These are Java classes which parsers output text and return problem matches. The project comes bundled with a regular expression parser, which is the simplest way to create a new parser. See the Javadoc for [`IBuildOutputParser`][parser_javadoc] and for [`RegexBuildOutputParser`][regex_parser_javadoc] for more information about that.
4. *Build entry files*: These are used to store information about builders for the *Add Program Builder* command (see below). They contain the XML elements for builders that are normally stored in the `.project` file in Eclipse projects. For an example of a build entry file, see `res/build_entries/ghc_builder_entry.xml`.

## Installing a bundled program builder with an error parser

Right click on the project on which you want the builder, choose *Configure* -> *Add Program Builder*. A file choosing dialog will open. Choose among the existing build entry files, they are the ones with "builder_entry" in their name. Finished.

This command adds external builders to the project, and the added builders are pre-configured to use output parsers. After this, when a *Build Project* command is issued the program builder will be called and its output sent to the parser.

A bundled builder that is added using the procedure is also a good starting point for users who want to add a builder for a compiler without a bundled builder entry file. Just add one of the bundled ones and change it around to fit your needs.

![Configure menu screenshot][configure_screenshot]

## Creating a new output parser

Two things are needed for this, and one thing is optional but useful.

1. An output parser. The simplest way is to use the bundled `RegexBuildOutputParser`. If the parsing is to complex to be easily done with a single regex it is be to instead write a custom parser. To do this, create a Java class that implements the `IBuildOutputParser` interface. (Put the plug-in jar file on the class path to get access to that interface.) For more information, look at the documentation for these two classes (see links above).
2. Create a parser configuration file. See the example in the `docs` directory and the bundled parser file in `res` for information about this.
3. (*Optional*) Create a build entry file. See the example and the bundled files for for information. This is only necessary if you want users to be able to easily add a builder using the *Add Program Builder* command (see above).

After these steps the builder is ready to be installed on projects, either manually through the Program Builder configuration dialog or using the *Add Program Builder* command.

## Alternative build markers

The Eclipse C/C++ Development Tools (CDT) has functionality for running custom external builders and parse their output for problem markers. Using this functionality separate from the rest of CDT might be an alternative to using the Laid Build Marker. However this functionality seems to be 1) too tangled with the rest the of CDT to be easily used without it, and 2) too complex too be easily understood. The Laid Build Marker is meant to provide a simple minimal effort solution that is easy to understand, use and customise.


## Troubleshooting

If no error markers appear when a project is build the following can help solve the problem.

* Check the console. Is the builder executed correctly and prints errors messages that should be parsed?
* Does the file locations that are printed in the console together with the *Parser output file* property (in the *Program Builder* -> *Problem Markers* configuration) give correct paths to the resources that should get the markers? This could be project relative paths or absolute paths.
* Check the *Error Log* view for error messages from the Build Marker plug-in.

## Tips and tricks

* Many compilers have a flag which enables the output of start and end offsets of errors. That information can be used by this plug-in. For example, give GHC the flag `-ferror-spans` to get underlined error regions in the Eclipse editors from the bundled parsers.
* The Eclipse workspace variables are very handy and can be used for most of the builder and parser configurations values. For example, `${build_project}` refers to the absolute path of the project being build, and `${resource_name}` to the name of the selected file. Learn more about them in the [External Tools section][tools_help_page] of the Eclipse help system.
* Custom workspace variables can be added in *Preferences* -> *Run/Debug* -> *String Substitution*.

## To-dos and ideas for future development

**Ideas**

* It might be a good idea to separate GUI parts in their own plug-in.
* Would it be a good idea with a "locked" config tab, for when builders are added programmatically?
* An easier way for lanugages designers to create specific configure commands, that add builders and launchers to a project. Maybe an extension point for this.

**To-dos**

* Make GUI for regex parsers.
* Re-write GHC parser in Java instead of regex.

## Author

Jens Lideström

[parser_javadoc]: https://bitbucket.org/lii/editor_utils_plugin/src/master/se.lidestrom.laid.build_marker/src/se/lidestrom/build_marker/parser/IBuildOutputParser.java
[regex_parser_javadoc]: https://bitbucket.org/lii/editor_utils_plugin/src/master/se.lidestrom.laid.build_marker/src/se/lidestrom/build_marker/parser/RegexBuildOutputParser.java
[editor_view_screenshot]: docs/Editor_view_markers_screenshot.png "Screenshot"
[configure_screenshot]: docs/Configure_meny_screenshot.png
[tools_help_page]: http://help.eclipse.org/juno/topic/org.eclipse.platform.doc.user/concepts/concepts-exttools.htm
[config_tab_screenshot]: docs/Config_tab_screenshot.png

