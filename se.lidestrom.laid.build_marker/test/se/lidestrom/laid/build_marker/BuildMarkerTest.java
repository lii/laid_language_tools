package se.lidestrom.laid.build_marker;

import static java.util.Collections.singletonMap;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import se.lidestrom.laid.build_marker.parser.IBuildOutputParser;
import se.lidestrom.laid.build_marker.parser.ProblemMatch;
import se.lidestrom.laid.build_marker.parser.RegexBuildOutputParser;
import se.lidestrom.laid.build_marker.parser.ProblemMatch.Severity;

public class BuildMarkerTest {

    @Test
    public void simpleRegexParser() {
        IBuildOutputParser p = new RegexBuildOutputParser(singletonMap("pattern", 
            "[^:]*:(?<file>\\w+):(?<line>\\d+):(?<column>\\d+):(?<severity>Warning):(?<message>\\w+):"));
        
        ProblemMatch r = p.parse("text \n text :file:1:2:Warning:Message: text text", false).get(0);
        assertEquals("file", r.resource);
        assertEquals(1, (int) r.line);
        assertEquals(2, (int) r.column);
        assertEquals(null, r.columnEnd);
        assertEquals(Severity.WARNING, r.severity);
        assertEquals("Message", r.message);
    }
    
    @Test
    public void haskellRegexParserSingleLine() {
        String output = "Comp/Env.hs:96:2: parse error on input ‘d’\n"
            + "Makefile:7: recipe for target 'source' failed";
        
        ProblemMatch r = new RegexBuildOutputParser(singletonMap("pattern", ParserFileGenerator.HASKELL_PATTERN))
            .parse(output, false).get(0);
        
        assertEquals("Comp/Env.hs", r.resource);
        assertEquals(96, (int) r.line);
        assertEquals(2, (int) r.column);
        assertEquals(Severity.ERROR, r.severity);
        assertEquals("parse error on input ‘d’", r.message);
    
    }
    
    @Test
    public void haskellRegexParserMultiline() {
        String output = 
            "unused rules: 1\n" +
            "shift/reduce conflicts:  1\n" +
            "\n" +
            "Comp/Expansion.hs:18:1:\n" +
            "    Couldn't match expected type ‘Program -> CompM Program’\n" +
            "                with actual type ‘CompM Program’\n" +
            "    The equation(s) for ‘expand’ have two arguments,\n" +
            "    but its type ‘Program -> CompM Program’ has only one\n" +
            "make: *** [source] Error 1\n" +
            "Comp/Expansion.hs:18:1:\n";
        
        ProblemMatch r = new RegexBuildOutputParser(singletonMap("pattern", ParserFileGenerator.HASKELL_PATTERN))
            .parse(output, false).get(0);
        
        assertEquals("Comp/Expansion.hs", r.resource);
        assertEquals(18, (int) r.line);
        assertEquals(1, (int) r.column);
        assertEquals(Severity.ERROR, r.severity);
        assertTrue(r.message.startsWith("Couldn't match expected type ‘Program -> CompM Program’"));
        assertTrue(r.message.endsWith("    but its type ‘Program -> CompM Program’ has only one"));
    }
    
    @Test
    public void haskellParserEndColumn() {
        String output = "test.hs:3:6-10: parse error on input `where'\n\n";
        
        ProblemMatch r = new RegexBuildOutputParser(
            singletonMap("pattern", ParserFileGenerator.HASKELL_PATTERN))
                .parse(output, false).get(0);
        
        assertEquals("test.hs", r.resource);
        assertEquals(3, (int) r.line);
        assertEquals(6, (int) r.column);
        assertEquals(10, (int) r.columnEnd);
        assertEquals(Severity.ERROR, r.severity);
        assertEquals("parse error on input `where'", r.message);
    }
    
    @Test
    public void haskellParserEndColumnMultiLine() {
        String output = 
            "test.hs:(5,4)-(6,7):\n" +
            "    Conflicting definitions for `a'\n" +
            "    Bound at: test.hs:5:4\n" +
            "              test.hs:6:7\n" +
            "    In the binding group for: a, b, a\n\n";
        
        ProblemMatch r = new RegexBuildOutputParser(
            singletonMap("pattern", ParserFileGenerator.HASKELL_PATTERN))
                .parse(output, false).get(0);
        
        assertEquals("test.hs", r.resource);
        assertEquals(5, (int) r.line);
        assertEquals(4, (int) r.column);
        assertEquals(6, (int) r.lineEnd);
        assertEquals(7, (int) r.columnEnd);
        assertEquals(Severity.ERROR, r.severity);
        assertTrue(r.message.startsWith("Conflicting definitions for"));
        assertTrue(r.message.endsWith("In the binding group for: a, b, a"));
    }
    
    @Test
    public void haskellSingleLineWarningRegexParser() {
        String output = 
            "Comp/Expansion.hs:14:3: Defined but not used: ‘i’\n" +
            "\n" +
            "Comp/Expansion.hs:15:3-4: Warning: Defined but not used: ‘i’\n" +
            "\n";
        
        List<ProblemMatch> rs = new RegexBuildOutputParser(singletonMap("pattern", ParserFileGenerator.HASKELL_PATTERN))
            .parse(output, false);
        
        assertEquals(2, rs.size());

        ProblemMatch r1 = rs.get(0);
        assertEquals("Comp/Expansion.hs", r1.resource);
        assertEquals(14, (int) r1.line);
        assertEquals(3, (int) r1.column);
//        assertEquals(Severity.WARNING, r1.severity);
        assertEquals("Defined but not used: ‘i’", r1.message);

        ProblemMatch r2 = rs.get(1);
        assertEquals("Comp/Expansion.hs", r2.resource);
        assertEquals(15, (int) r2.line);
        assertEquals(3, (int) r2.column);
        assertEquals(Severity.WARNING, r2.severity);
        assertEquals("Defined but not used: ‘i’", r2.message);
    }

    @Test
    public void stringCombiner() {
        assertEquals(Arrays.asList("a", "b"), Utils.separate(';', '\\', "a;b"));
        assertEquals(Arrays.asList(), Utils.separate(';', '\\', ""));
        assertEquals(Arrays.asList(";", "a"), Utils.separate(';', '\\', "\\;;a"));
        assertEquals(Arrays.asList("", ""), Utils.separate(';', '\\', ";"));
        assertEquals(Arrays.asList(""), Utils.separate(';', '\\', "\\"));
        assertEquals(Arrays.asList("a", ""), Utils.separate(';', '\\', "a;"));
        assertEquals(Arrays.asList("", "a"), Utils.separate(';', '\\', ";a"));

        assertEquals("a;b", Utils.combine(';', '\\', Arrays.asList("a", "b")));
        assertEquals("", Utils.combine(';', '\\', Arrays.asList()));
        assertEquals("\\;;a", Utils.combine(';', '\\', Arrays.asList(";", "a")));
        assertEquals(";", Utils.combine(';', '\\', Arrays.asList("", "")));
        assertEquals("\\", Utils.combine(';', '\\', Arrays.asList("")));
        assertEquals("\\", Utils.combine(';', '\\', Arrays.asList("")));
    }

}
