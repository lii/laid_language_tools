package se.lidestrom.laid.build_marker;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import se.lidestrom.laid.build_marker.BuildMarkerPlugin.StreamSelection;


public class ParserFileGenerator {
    
    private static final String FILE = "test_parser.xml";

    public static String TEST_LANG_PATTERN = "^(?<file>\\w+):(?<line>\\d+):(?<column>\\d+):(?<message>.*)";

    public static String HASKELL_PATTERN = 
        // Match out attributes separate by ':' one one line, then get the rest as "message". 
        "(?sm)" + // Set multi-line and dotall options
        "^(?<file>[^:\\n\\r]+)" +
        ":\\(?(?<line>\\d+)[:,](?<column>\\d+)\\)?(-(\\((?<lineEnd>\\d+),)?(?<columnEnd>\\d+)\\)?)?" + // Line that starts with file, then line nr
//        "|\\((?<line>\\d+),(?<column>\\d+)\\)-\\((?<lineEnd>\\d+),(?<columnEnd>\\d+)\\))" + 
        ":\\s?(?<severity>Warning)?:?\\W*" // Capture Warning tag if there is one
        + "(?<message>.+?)" // All characters, with a reluctant matching to avoid getting next line 
        + "((!?[\\n\\r][\\n\\r])|(!?[\\n\\r]\\S))"; // End on empty line or non-indented line

    
    public static void read() throws JAXBException {
        Unmarshaller unmarshaller =
            JAXBContext.newInstance(ParserConfig.class).createUnmarshaller();
        ParserConfig c = (ParserConfig) unmarshaller.unmarshal(new File(FILE));

        System.out.println(c.className);
    }
    
    public static void writeConfig() throws JAXBException {
        Map<String, String> args = new LinkedHashMap<>();
        args.put("patten", HASKELL_PATTERN);
        args.put("some_arg", "some_arg_value");
        
        ParserConfig c = new ParserConfig(
            Optional.empty(), "some.Parser", StreamSelection.STDERR, true, args, 
            Optional.of("Parser.java"));
        
        Marshaller marshaller =
            JAXBContext.newInstance(ParserConfig.class).createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        
        marshaller.marshal(c, System.out);
    }
    
    public static void main(String[] args) throws JAXBException {
        writeConfig();
//        read();
    }

}



