package se.lidestrom.laid.build_marker;

import static java.util.Collections.singletonList;

import java.util.List;
import java.util.Map;

import se.lidestrom.laid.build_marker.parser.IBuildOutputParser;
import se.lidestrom.laid.build_marker.parser.ProblemMatch;

/**
 * Used to test parser loading.
 */
public class PathBuildOutputParser implements IBuildOutputParser {
    private int i = 1;
    
    public PathBuildOutputParser(Map<String, String> m) {
        System.out.println("BuildOutputParser ctor m: " + m);
    }
    
    @Override
    public List<ProblemMatch> parse(String arg0, boolean arg1) {
        ProblemMatch m = new ProblemMatch();
        m.message = "Path Match " + i++;
        return singletonList(m);
    }
}
