
The active contribution item identifier:
org.eclipse.m2e.enableNatureAction
The active contribution location URI:
menu:org.eclipse.ui.projectConfigure?after=org.eclipse.m2e.enableNatureAction
The active contribution item class:
EnableNatureAction
The contributing plug-in:
org.eclipse.ui.workbench (3.107.1.v20160120-2131)


Add nature:

     IProjectDescription description = project.getDescription();
     String[] prevNatures = description.getNatureIds();
     String[] newNatures = new String[prevNatures.length + 1];
     System.arraycopy(prevNatures, 0, newNatures, 0, prevNatures.length);
     newNatures[prevNatures.length] = ProjectNature.NATURE_ID;
     description.setNatureIds(newNatures);

     IProgressMonitor monitor = null;
     project.setDescription(description, monitor);
     
     

The console never misses output. How? Flush something? StreamMonitor.getContents?

Could maybe get project from dynamic vars? No, that is static data, only valid when a build in being constructed.


Bundle bundle = Platform.getBundle(yourPluginId);
Path path = new Path("icons/sample.gif");
URL fileURL = FileLocator.find(bundle, path, null);
InputStream in = fileURL.openStream();


Load classes: 
Make them into a Jar bundle and load them with OSGi
https://stackoverflow.com/questions/4830037/adding-jars-to-an-eclipse-plugin-at-runtime

To do
=====

* Finish documentation.
* Make separate feature and add to update site
* Add more bundled parsers?

Done
----

* Read and write parser information from launch config. 
* Handle case that not all attribute groups are present in regex for parser.


<mapAttribute key="se.lidestrom.laid.build_marker.PARSER_DATA_ATTR">
  <mapEntry key="stream" value="stderr"/>
  <mapEntry key="parserClass" value="se.lidestrom.laid.build_marker.RegexBuildOutputParser"/>
  <mapEntry key="filePrefix" value="src/"/> 
</mapAttribute>
<mapAttribute key="se.lidestrom.laid.build_marker.PARSER_ARGS_ATTR">
  <mapEntry key="pattern" value="(?sm)^(?&lt;file&gt;[^:&#xA;&#xD;]+):(?&lt;line&gt;\d+):(?&lt;column&gt;\d+):\s?(?&lt;warning&gt;Warning)?:?\W*(?&lt;message&gt;.+?)((!?[\n\r][\n\r])|(!?[\n\r]\S))"/>
</mapAttribute>

