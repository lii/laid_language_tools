package se.lidestrom.laid.build_marker;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.runtime.CoreException;

import se.lidestrom.editor_util.OptionalAdapter;
import se.lidestrom.laid.build_marker.BuildMarkerPlugin.StreamSelection;

/**
 * Contains information about a parser. Where to find its class file, its argument,
 * which out streams to parser, and other things.
 * <p/>
 * This information is saved in a parser config file, which is referenced by path from
 * the build config.
 */
@XmlRootElement
public class ParserConfig {

    @XmlAttribute
    @XmlJavaTypeAdapter(OptionalStringAdapter.class)
    public final Optional<String> filePrefix;
    
    @XmlAttribute(required = true)
    public final String className;

    @XmlAttribute(required = true)
    public final StreamSelection stream;

    @XmlAttribute
    @XmlJavaTypeAdapter(OptionalStringAdapter.class)
    public final Optional<String> classPath;
    
    @XmlElementWrapper
    public final Map<String, String> arguments;
    
    public ParserConfig(Optional<String> filePrefix, String className, StreamSelection stream, boolean enabled, Map<String, String> arguments, Optional<String> classPath) {
        this.filePrefix = filePrefix;
        this.className = className;
        this.stream = stream;
        this.arguments = arguments;
        this.classPath = classPath;
    }
    
    
    // Used by XML parser library
    @SuppressWarnings("unused")
    private ParserConfig() {
        this.filePrefix = Optional.empty();
        this.className = null;
        this.stream = null;
        this.arguments = new LinkedHashMap<>();
        this.classPath = Optional.empty();
    }

    /**
     * Loads and returns a ParserConfig which information is saved in the file on the path
     * parserConfigFile.
     */
    public static ParserConfig loadFromFile(String parserConfigFile, IContainer resolvePath) {
        try {
            return (ParserConfig) JAXBContext.newInstance(ParserConfig.class)
                .createUnmarshaller()
                .unmarshal(Utils.resolveResourceOrPathToPath(parserConfigFile, resolvePath).toFile());
        } catch (JAXBException | CoreException exc) {
            throw new RuntimeException(exc);
        }
    }
    
    public static class OptionalStringAdapter extends OptionalAdapter<String> {}
}
