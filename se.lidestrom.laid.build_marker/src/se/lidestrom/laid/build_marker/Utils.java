package se.lidestrom.laid.build_marker;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.variables.VariablesPlugin;

import se.lidestrom.editor_util.FunctionUtils.SpecificThrowingConsumer;

/**
 * Utils used only in the build marker project.
 */
public class Utils {
    
    private Utils() {}
    
    /**
     * Combines all strings in toBeCombined, using separatorChar, escaping all occurences of
     * separatorChar in string by escapeChar. This is the inverse of {@link Utils#separate}.
     */
    public static String combine(char separatorChar, char escapeChar, Collection<String> toBeCombined) {
        if (toBeCombined.size() == 1 && toBeCombined.iterator().next().isEmpty()) {
            return "" + escapeChar;
        }

        StringBuilder builder = new StringBuilder();
        boolean isFirst = true;
        
        for (String s : toBeCombined) {
            if (isFirst) isFirst = false;
            else builder.append(separatorChar);
            
            for (int i = 0; i < s.length(); i++) {
                char ch = s.charAt(i);
                if (ch == separatorChar || ch == escapeChar) builder.append(escapeChar);
                builder.append(ch);
            }
        }
        
        return builder.toString();
    }
    
    /**
     * Divides substrings in combinedString separated by separatorChar, also removes escaped
     * occurences of separatorChar. This is the inverse of {@link Utils#combine}.
     */
    public static ArrayList<String> separate(char separatorChar, char escapeChar, String combinedString) {

        if (combinedString.isEmpty()) return new ArrayList<>();
        // Special case for single empty string
        if (combinedString.charAt(0) == escapeChar && combinedString.length() == 1) return new ArrayList<>(Arrays.asList("")); 

        ArrayList<String> separateStrings = new ArrayList<>();
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < combinedString.length(); i++) {
            char ch = combinedString.charAt(i);
            if (ch == escapeChar) {
                // Check next char, handle errors append if valid
                i++;
                if (i == combinedString.length()) throw new IllegalArgumentException("Escape character at end of input");
                ch = combinedString.charAt(i + 1);
                if (ch != escapeChar && ch != separatorChar) throw new IllegalArgumentException("Invalid escaped char at index " + i + ":" + ch);
                builder.append(ch);
            } else if (ch == separatorChar) {
                // String finished, prepare for next one
                separateStrings.add(builder.toString());
                builder.setLength(0);
            } else {
                // Normal char, continue with the same string
                builder.append(ch);
            }
        }
        
        separateStrings.add(builder.toString());
        
        return separateStrings;
    }
    
    /**
     * 
     * @param charsToEscape
     * @param escapeChar
     * @param input
     * @return
     */
    public static String escape(String charsToEscape, char escapeChar, String input) {
        StringBuilder builder = new StringBuilder();
        
        for (int i = 0; i < input.length(); i++) {
            char ch = input.charAt(i);
            if (charsToEscape.indexOf(ch) != -1 || ch == escapeChar) {
                // Check next char, handle errors append if valid
                i++;
                if (i == input.length()) throw new IllegalArgumentException("Escape character at end of input");
                ch = input.charAt(i + 1);
                if (charsToEscape.indexOf(ch) != -1 || ch == escapeChar) throw new IllegalArgumentException("Invalid escaped char at index " + i + ":" + ch);
                builder.append(escapeChar);
            }
            builder.append(ch);
        }
        
        return builder.toString();
    }

    public static String unescape(String charsToEscape, char escapeChar, String text) {
        StringBuilder builder = new StringBuilder();
        
        for (int i = 0; i < text.length(); i++) {
            char ch = text.charAt(i);
            if (charsToEscape.indexOf(ch) != -1 || ch == escapeChar) {
                // Check next char, handle errors append if valid
                i++;
                if (i == text.length()) throw new IllegalArgumentException("Escape character at end of input");
                ch = text.charAt(i + 1);
                if (charsToEscape.indexOf(ch) != -1 || ch == escapeChar) throw new IllegalArgumentException("Invalid escaped char at index " + i + ":" + ch);
            }
            builder.append(ch);
        }
        
        return builder.toString();
    }
    
    @SuppressWarnings("unchecked")
    public static <E extends Throwable> void sneakyThrow(Throwable e) throws E {
        throw (E) e;
    }
    
    /**
     * Calls c with each element in iterable. If c throws a exceptionClass
     * exception for any of the elements that exception is saved and the
     * iteration continues. When the iteration finishes an exception created by
     * combinedExceptionCreator will get thrown if any exception was throws
     * earlier. The previous exception will be added as suppressed exceptions.
     */
    public static <ELEM, EXC_CAUGHT extends Throwable, EXC_THROWN extends Throwable> void forEachSupressExceptions(Iterable<ELEM> iterable, 
        Class<EXC_CAUGHT> exceptionClass,
        Function<String, EXC_THROWN> combinedExceptionCreator,
        SpecificThrowingConsumer<? super ELEM, EXC_CAUGHT> c) throws EXC_THROWN {
        List<EXC_CAUGHT> thrownExceptions = null;
        for (ELEM e : iterable) {
            try {
                c.accept(e);
            } catch (Throwable t) {
                if (exceptionClass.isInstance(t)) {
                    if (thrownExceptions == null) thrownExceptions = new ArrayList<>();
                    thrownExceptions.add(exceptionClass.cast(t));
                } else {
                    if (t instanceof Error) throw (Error) t;
                    else if (t instanceof RuntimeException) throw (RuntimeException) t;
                    else throw new RuntimeException("Unexpected exception in forEachSupressExceptions", t);
                }
            }
        }
        
        if (thrownExceptions != null) {
            EXC_THROWN exc = combinedExceptionCreator.apply("Combined exception from forEachSupressExceptions");
            for (EXC_CAUGHT e : thrownExceptions) exc.addSuppressed(e);
            throw exc;
        }
        
    }

    /**
     * First performs string substitution on the argument. Then tries to resolve the it 
     * relative to {@code resolveAgainst}, otherwise returns it as-is.
     */
    public static IPath resolveResourceOrPathToPath(String path, IContainer resolveAgainst) throws CoreException {
        // ${project} variable is used and wrong project is active?
        IPath resolvedFile = new Path(VariablesPlugin.getDefault().getStringVariableManager()
            .performStringSubstitution(path));
     
        if (resolveAgainst == null) return resolvedFile;
        
        IResource resource = resolveAgainst.findMember(resolvedFile);
        
        if (resource != null) return resource.getLocation();
        
        // Handle locations inside bundles
        // Not used because it turned out to be tricky and I'm not sure if it is a good idea.
        //
        //        IPath bundleLoc =  new Path(bundle.getLocation());
        //        
        //        if (bundleLoc.isPrefixOf(resolvedFile)) {
        //            return new Path(FileLocator.toFileURL(bundle.getEntry(
        //                resolvedFile.makeRelativeTo(bundleLoc).toString())).getPath());
        //        }
        
        return resolvedFile;
    }
}






