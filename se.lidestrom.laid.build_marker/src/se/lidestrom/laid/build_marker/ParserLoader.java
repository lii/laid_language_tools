package se.lidestrom.laid.build_marker;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.InvalidRegistryObjectException;
import org.eclipse.core.runtime.Platform;

import se.lidestrom.editor_util.Result;
import se.lidestrom.laid.build_marker.BuildMarkerPlugin.StreamSelection;
import se.lidestrom.laid.build_marker.parser.IBuildOutputParser;

/**
 * Loads parser classes and create parser object from them. Classes can be loaded from either
 * project classpath, extension points or URLs.
 */
public class ParserLoader {
    
    public static class ParserCreators {
        public final List<Supplier<IBuildOutputParser>> stdoutCreators;
        public final List<Supplier<IBuildOutputParser>> stderrCreators;

        public ParserCreators() {
            this.stdoutCreators = new ArrayList<>();
            this.stderrCreators = new ArrayList<>();
        }
    }

    /**
     * Returns the parsers that are set for the provided ParserConfig.
     */
	public ParserCreators getParsers(ParserConfig config, IContainer project) throws CoreException {
        ParserCreators parserCreators = new ParserCreators();

        Result<Class<IBuildOutputParser>> cls = loadClass(config.className, config.classPath, project);

        if (!cls.isSuccess()) {
            BuildMarkerPlugin.logWarning(cls.getErrorMessage("; "));
            return parserCreators;
        }
        
        Constructor<?>[] ctors = cls.get().getConstructors();
        if (ctors.length != 1) throw new IllegalStateException(
            cls.get().getName() + " should have only one public contstructor but has " + ctors.length + ".");
        
        Constructor<?> ctor = ctors[0];

        Map<String, String> argMap = config.arguments == null
            ? new HashMap<>() : config.arguments;

        if (!argMap.isEmpty() && ctor.getParameterCount() == 0) {
            throw new IllegalStateException("Trying to send arguments to argumentless parser: " + cls.get().getName());
        }
        
        Object[] args = ctor.getParameterCount() == 0
            ? new Object[0] : new Object[] { config.arguments };
        
        Supplier<IBuildOutputParser> s = () -> {
                try {
                    return (IBuildOutputParser) ctor.newInstance(args);
                } catch (ReflectiveOperationException e) {
                    throw new RuntimeException(e);
                }
            };

        if (config.stream == StreamSelection.STDOUT || config.stream == StreamSelection.BOTH) {
            parserCreators.stdoutCreators.add(s);
        }
        if (config.stream == StreamSelection.STDERR || config.stream == StreamSelection.BOTH) {
            parserCreators.stderrCreators.add(s);
        }

        return parserCreators;
    }

    private static  Result<Class<IBuildOutputParser>> loadClass(String className, Optional<String> fileName, IContainer project) {
        // Get result from file if there is a file name, else from the first to succeed
        // of classpath and extension point 
        Result<Class<?>> result = fileName.isPresent()
                ? loadParserFromFile(fileName.get(), className, project)
                : loadParserFromClasspath(className).tryIfFailure(() -> 
                    loadParserFromExtensionPoint(className));
        // Then try to cast result to IBuildOutputParser
        return result.continueIfSuccess((Class<?> cls) -> IBuildOutputParser.class.isAssignableFrom(cls)
                ? Result.success(cls.asSubclass(IBuildOutputParser.class))
                : Result.failure("The loaded class does not implement " + IBuildOutputParser.class.getName()));
    }

    private static Result<Class<?>> loadParserFromFile(String parserPath, String parserClassName, IContainer project) {
        try {
            IPath p = Utils.resolveResourceOrPathToPath(parserPath, project);

            // Resolve relative file path using project path 
            try (URLClassLoader classLoader = URLClassLoader.newInstance(
                new URL[] {p.toFile().toURI().toURL()}, 
                ParserLoader.class.getClassLoader())) {

            	return Result.success(classLoader.loadClass(parserClassName));
            }
        } catch (ClassNotFoundException | CoreException | IOException exc) {
            return Result.failure("Failed to load class " + parserClassName + " from location " + parserPath + " with message " + exc);
        }
    }
        
    private static Result<Class<?>> loadParserFromExtensionPoint(String className) {        
        IConfigurationElement e = Stream.of(Platform.getExtensionRegistry()
                .getConfigurationElementsFor(BuildMarkerPlugin.PARSERS_EXTENSION_POINT_ID))
            .filter(ce -> ce.getAttribute("class").equals(className))
            .findFirst()
            .orElse(null);
        
        if (e == null) {
            return Result.failure("No class with name " + className + " is contributed to extension point " + BuildMarkerPlugin.PARSERS_EXTENSION_POINT_ID);
        }
        
        try {
            Class<?> cls = Platform.getBundle(e.getDeclaringExtension().getContributor().getName())
                .loadClass(e.getAttribute("class"));
            return Result.success(cls);
        } catch (ClassNotFoundException | InvalidRegistryObjectException exc) {
            return Result.failure("Could not load class " + className + " from extension point. Message: " + exc);
        }
    }
    
    private static Result<Class<?>> loadParserFromClasspath(String className) {        
        try {
            return Result.success(BuildMarkerPlugin.getDefault().getBundle().loadClass(className)); 
        } catch (ClassNotFoundException exc) {
            return Result.failure("Class " + className + " not found on classpath");
        }
        
    }

    
}
