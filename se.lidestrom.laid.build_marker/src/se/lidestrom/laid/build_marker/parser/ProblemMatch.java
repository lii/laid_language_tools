package se.lidestrom.laid.build_marker.parser;

import org.eclipse.core.resources.IMarker;

/**
 * Contains information about one issue found in the output of a builder. It is returned
 * by a output parser.
 * <p/>
 * All values are nullable in which case markers get empty values or default values.
 */
public class ProblemMatch {
    /**
     * The resource a problem marker should be added to. Resolved first as a
     * project relative path, if that fails as a absolute path (which must lead
     * to inside the build project).
     * <p>
     * The filePrefix from the builder configuration will be prefixed to this. If null 
     * markers are set on the project being built.
     */
    public String resource = null;
    
    /**
     * Decides how Eclipse reports the match.
     */
    public Severity severity = null;

    /**
     * Decides the error message in the Problems view.
     */
    public String message = null;

    /* * * * * * * * * * * * */
    // Position properties. If none of these are set the build project is used as location.
    // A subset of them can be set, setting more of them gives more accurate marker position.
    // If one is set, then the ones above it must also be set for it to have any effect.
    /* * * * * * * * * * * * */

    
    /**
     * 1-indexed line number
     */
    public Integer line = null;

    /**
     * 1-indexed column number in the given line
     */
    public Integer column = null;

    /**
     * 1-indexed line number of the end of the match for a multi-line match. 
     */
    public Integer lineEnd = null;

    /**
     * 1-indexed column number in the given end line
     */
    public Integer columnEnd = null;

    /* * * * * * * * * * * * */


    public enum Severity {
        ERROR(IMarker.SEVERITY_ERROR), 
        WARNING(IMarker.SEVERITY_WARNING),
        INFO(IMarker.SEVERITY_INFO);
        
        private Severity(int eclipseValue) {
            this.eclipseValue = eclipseValue;
        }
        
        public final int eclipseValue;
    }
}

