package se.lidestrom.laid.build_marker.parser;

import java.util.List;

/**
 * The interface that is implemented by clients to parse the output of some particular
 * build program. It is called every time there is new output from the builder. The matches 
 * that are returned are turned into Problem markers in the Eclipse editors and Problems view. 
 */
public interface IBuildOutputParser {
    /**
     * @param output The latest output from the build program, or null if terminating is true.
     * @param isLastCall Set to true to indicate that build problem has terminated and there
     *  will be no more output. Can be used to trigger flush of cached data.
     * @return Output matches that should be turned into Problem markers.
     */
    public List<ProblemMatch> parse(String output, boolean isLastCall);
}
