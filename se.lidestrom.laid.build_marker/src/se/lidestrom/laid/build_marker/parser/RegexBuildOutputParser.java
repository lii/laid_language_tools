package se.lidestrom.laid.build_marker.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import se.lidestrom.laid.build_marker.BuildMarkerPlugin;
import se.lidestrom.laid.build_marker.parser.ProblemMatch.Severity;

/**
 * This class is a IBuildOutputParser which uses a regex to match builder output. Each match
 * of the regex is turned into an problem marker. The properties of the marker is taken from
 * named groups in the regex.
 * <p/>
 * The argument map must contain the "pattern" key with the regex pattern. It migh also contain
 * the "lineStartIndex" and "columnStartIndex" keys, mapping to integers. They default to 1.
 * <p/>
 * The regex might contain the following named groups: "file", "line", "column", "message", "severity".
 * If any of these groups are missing default values are used, for example error markers
 * are placed on the project directly if the "file" group is missing. For more details about these
 * fields, see the {@link ProblemMatch} class.
 * <p/>
 * For example, the following regex matches the output of some compilers: 
 * {@code ^(?<file>\w+):(?<line>\d+):(?<column>\d+):(?<message>.*)}
 * <p/>
 * Note that if multi-line matches are desired this option to Java regexes can be given in the 
 * start of the pattern. Example: {@code (?sm)^(?<file> ...}
 * <p/>
 * This is a convenient way to create a parser for very simple build output. If the output
 * is complicated it is probably easier to write a parser in Java that in a single regex.
 */
public class RegexBuildOutputParser implements IBuildOutputParser {
    
    public static final String MESSAGE_GROUP = "message";
    public static final String FILE_GROUP = "file";
    public static final String LINE_END_GROUP = "lineEnd";
    public static final String LINE_GROUP = "line";
    public static final String COLUMN_END_GROUP = "columnEnd";
    public static final String COLUMN_GROUP = "column";

    public static final String SEVERITY_GROUP = "severity";
    public static final String WARNING = "warning";
    public static final String ERROR = "error";
	
	private StringBuilder outputCache = new StringBuilder();
    private final Matcher errorMatcher;
    private final int lineStartIndex;
    private final int columnStartIndex;
    
    /**
     * Input is given as a map with a single entry: "pattern" -> <regex pattern>
     */
    public RegexBuildOutputParser(Map<String, String> args) {
        // If pattern contains only workspace variables then perform variable substitution. 
        String pattern = args.get("pattern");
        if (pattern == null) {
            throw new IllegalArgumentException("RegexBuildOutputParser was created without a \"pattern\" argument. The provided arguments were: " + args);
        }
        
        lineStartIndex = Integer.parseInt(args.getOrDefault("lineStartIndex", "1"));
        columnStartIndex = Integer.parseInt(args.getOrDefault("columnStartIndex", "1"));
        
        this.errorMatcher = Pattern.compile(pattern).matcher(outputCache);
    }
    
    @Override
    public List<ProblemMatch> parse(String output, boolean terminating) {
        outputCache.append(terminating ? "\n\n" : output);
        errorMatcher.reset(outputCache);

        List<ProblemMatch> foundMarkers = new ArrayList<>();
        int lastMatchEnd = -1;
        
        while (errorMatcher.find()) {
            foundMarkers.add(buildMatch());
            
            // Record end to be able to remove text up to that point
            lastMatchEnd = errorMatcher.end();
        }
        
        // If there was a match then the output cache should be reset
        if (lastMatchEnd != -1) {
            if (lastMatchEnd < outputCache.length()) {
                // Switch cache to an new builder containing the text after the match
                StringBuilder newOutputCache = new StringBuilder();
                newOutputCache.append(outputCache, lastMatchEnd, outputCache.length());
                outputCache = newOutputCache;
            } else if (lastMatchEnd == outputCache.length()) {
                outputCache.setLength(0);
            }
        }
        
        return foundMarkers;
    }

    private ProblemMatch buildMatch() {
        ProblemMatch m = new ProblemMatch();

        // Set severity, default is ERROR
        String severity = getGroupGuarded(errorMatcher, SEVERITY_GROUP);
        if (severity == null || severity.isEmpty() || severity.equalsIgnoreCase(ERROR)) m.severity = Severity.ERROR;
        else if (severity.equalsIgnoreCase(WARNING)) m.severity = Severity.WARNING;
        else BuildMarkerPlugin.logWarning("Regex parser encounterd an invalid match for group \"" + SEVERITY_GROUP + "\": " + severity);
        
        // Set location attributes if they are present in the match
        m.line = parseIntToNullable(getGroupGuarded(errorMatcher, LINE_GROUP));
        if (m.line != null) {
            // Adjust for custom start index
            m.line += 1 - lineStartIndex;
            m.column = parseIntToNullable(getGroupGuarded(errorMatcher, COLUMN_GROUP));
            
            if (m.column != null) {
                // Adjust for custom start index
                m.column += 1 - columnStartIndex;
                m.columnEnd = parseIntToNullable(getGroupGuarded(errorMatcher, COLUMN_END_GROUP));
       
                if (m.columnEnd == null) {
                    // Make a 1 column wide marker, since GHC doesn't output end column 
                    // for that width.
                    m.columnEnd = m.column + 1;
                } else {
                    m.lineEnd = parseIntToNullable(getGroupGuarded(errorMatcher, LINE_END_GROUP));
                }
            }
        }
                
        m.resource = getGroupGuarded(errorMatcher, FILE_GROUP);
        m.message = getGroupGuarded(errorMatcher, MESSAGE_GROUP); 
        
        return m;
    }
    
    private static String getGroupGuarded(Matcher matcher, String groupName) {
        try {
            return matcher.group(groupName);
        } catch (IllegalArgumentException e) {
            // IllegalArgumentException is thrown when groupName does not exist in the pattern.
            // This is a slightly ugly trick to enable user to omit groups in their pattern. 
            return null;
        }
    }
    
    private static Integer parseIntToNullable(String s) {
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return null;
        }
        
    }

}
