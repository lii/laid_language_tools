package se.lidestrom.laid.build_marker;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ui.IPartListener2;
import org.eclipse.ui.IWindowListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.ITextEditor;

import se.lidestrom.editor_util.EditorHelper;
import se.lidestrom.editor_util.FunctionUtils;

/**
 * Updates the location of markers. Called when build finishes for open editors, and also when 
 * editors are opened.
 * <p/>
 * Markers a set on a given resource when they are created. But markers keep their location
 * as line-and column and documents handle location by offset from document start. The
 * translation between the two formats are handled by this class.
 * <p/>
 * This is done by listening for opened files and checking if they have any build markers.
 */
public class MarkerRegionUpdater {

    /**
     * Returns a IWindowListener that updates marker locations when editors are
     * opened.
     */
    public static IWindowListener getWindowListener() {
        // One listener to update markers when editors are opened
        IPartListener2 partListener = new IPartListener2() {
            @Override
            public void partOpened(IWorkbenchPartReference partRef) {
                IWorkbenchPart part = partRef.getPart(false);
                
                if (part == null) return;
                
                EditorHelper helper = EditorHelper.createFromPart(part);
                if (!helper.isTextEditor()) return;

                Optional<IFile> res = helper.getEditorFile();
                if (!res.isPresent()) return;

                updateMarkerPositions(res.get(), helper.getEditor());
            }

            @Override public void partActivated(IWorkbenchPartReference partRef) {}
            @Override public void partBroughtToTop(IWorkbenchPartReference partRef) {}
            @Override public void partClosed(IWorkbenchPartReference partRef) {}
            @Override public void partDeactivated(IWorkbenchPartReference partRef) {}
            @Override public void partHidden(IWorkbenchPartReference partRef) {}
            @Override public void partVisible(IWorkbenchPartReference partRef) {}
            @Override public void partInputChanged(IWorkbenchPartReference partRef) {}
        };
        
        // One listener to register the first once the window is opened.
        // Have to do this because windows does not exist when plugin is started.
        return new IWindowListener() {

            private void addAndRemoveListeners(IPartListener2 partList, IWorkbenchWindow window) {
                window.getPartService().addPartListener(partList);
                // This breaks on multiple windows. But the only consequece will be that
                // secondary windows won't get region updates and I can't stand the
                // thougt of having a listener unnessesarily called on *every* windows 
                // actiontion!
                PlatformUI.getWorkbench().removeWindowListener(this);
            }
            
            @Override
            public void windowOpened(IWorkbenchWindow window) {
                addAndRemoveListeners(partListener, window);
            }

            
            @Override
            public void windowActivated(IWorkbenchWindow window) {
                addAndRemoveListeners(partListener, window);
            }
            
            @Override public void windowDeactivated(IWorkbenchWindow window) {}
            @Override public void windowClosed(IWorkbenchWindow window) {}
        };
    }
    
    /**
     * Updates build problem markers of resourceToUpdate by using the document of
     * editorOfResource.
     */
    public static void updateMarkerPositions(IResource resourceToUpdate, ITextEditor editorOfResource) {
            IDocument document = editorOfResource.getDocumentProvider().getDocument(editorOfResource.getEditorInput());
            List<IMarker> markers = Arrays.asList(
                FunctionUtils.runtimize(() -> resourceToUpdate.findMarkers(BuildMarkerPlugin.MARKER_ID, true, 1)));

            if (markers.stream().anyMatch(m -> m.getAttribute(IMarker.CHAR_START, -1) != -1)) {
                // If any of the markers has this attribute then all of them have 
                // already been updated
                return;
            }
            
            Utils.forEachSupressExceptions(markers, Exception.class, RuntimeException::new,
                marker -> {
                    // Markers are 1-indexed and document 0-indexed. Must convert line back-and-forth
                    int column = marker.getAttribute(BuildMarkerPlugin.MARKER_COLUMN_ATTR, -1) - 1; // Markers are 1-indexed
                    int line = marker.getAttribute(IMarker.LINE_NUMBER, -1) - 1; // Markers are 1-indexed
                    
                    if (line != -1) {
                        int charStart = document.getLineOffset(line) + column;

                        // Only set marker if it is inside document, otherwise it will dissapear
                        if (charStart < document.getLength()) {
                            marker.setAttribute(IMarker.CHAR_START, charStart);
                        
                            // Markers are 1-indexed and document 0-indexed. Must convert line back-and-forth
                            int lineEnd = marker.getAttribute(BuildMarkerPlugin.MARKER_LINE_END_ATTR, line + 1) - 1;
                            // I don't understand why there is no -1 here, but it seems to work
                            int columnEnd = marker.getAttribute(BuildMarkerPlugin.MARKER_COLUMN_END_ATTR, -1);
                            
                            if (columnEnd == -1) {
                                marker.setAttribute(IMarker.CHAR_END, charStart);
                            } else {
                                int charEnd = document.getLineOffset(lineEnd) + columnEnd;
                                marker.setAttribute(IMarker.CHAR_END, charEnd);
                            }
                        }

                    }
                });
    }
}
