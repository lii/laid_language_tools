package se.lidestrom.laid.build_marker;

import static java.util.Collections.emptyList;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.debug.core.IStreamListener;
import org.eclipse.debug.core.model.IStreamMonitor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.ITextEditor;

import se.lidestrom.editor_util.EditorUtils;
import se.lidestrom.laid.build_marker.parser.IBuildOutputParser;
import se.lidestrom.laid.build_marker.parser.ProblemMatch;

/**
 * Set on process out stream. Passes output to output parsers and makes problem markers of
 * their return value.
 */
class OutputParserStreamListener implements IStreamListener {
    private final Collection<IBuildOutputParser> parsers;
    private final IContainer project;
    private final Set<IResource> filesWithMarkers = new HashSet<>();
    private final String builderId;
    private final String filePrefix;
    
    public OutputParserStreamListener(Collection<IBuildOutputParser> parsers, IContainer project, String builderId, String filePrefix) {
        this.parsers = parsers;
        this.project = project;
        this.builderId = builderId;
        this.filePrefix = filePrefix;
    }
    
    @Override
    public void streamAppended(String output, IStreamMonitor monitor) {
        runProcessors(output, false);
    }

    private void runProcessors(String output, boolean isLastCall) {
            Utils.forEachSupressExceptions(parsers, Exception.class, RuntimeException::new,
                (IBuildOutputParser parser) -> {
                    for (ProblemMatch problemMatch : parser.parse(output, isLastCall)) {
                        IResource file = resolveResource(problemMatch.resource);
                        if (file != null) {
                            IMarker marker = file.createMarker(BuildMarkerPlugin.MARKER_ID);
                            fillMarker(problemMatch, marker);
                            marker.setAttribute(BuildMarkerPlugin.MARKER_BUILDER_NAME_ATTR, builderId);
                            filesWithMarkers.add(file);
                        }
                    }
                }
            );
    }

    // NOTE: If this procedure is changed, update documentation in: ProblemMatch,
    // ProblemMarkersLaunchConfigurationTab and instruction files.
    private IResource resolveResource(String resourceToMark) {

        // If no resource is set, use project as default
       if (resourceToMark == null) return project;
        
        // Add prefix
        IPath resourcePath = new Path((filePrefix.isEmpty() ? "" : filePrefix + "/") + resourceToMark);

        // Lookup resource relative to the project
        IResource file = project.findMember(resourcePath);

        if (file != null) return file;
            
        // If that fails, lookup resource as an absolute path
        if (project.getLocation().isPrefixOf(resourcePath)) {
                return project.findMember(resourcePath.makeRelativeTo(project.getLocation()));
        }
        
        BuildMarkerPlugin.logWarning("A build output parser returned a path to a non-existing resource. The resource path from the parser together with the prefix must be a project relative path or an absolute file system path to a resource within the built project."
            + " projectPath: " + project + " File prefix: " + filePrefix + " Returned resource: " + resourceToMark);
        
        return null;
    }

    public void buildTerminated() {
        runProcessors(null, true);
        
        // Update marker with info from open editors if there are any
        // (the rest if updated when editors are opened)
        if (filesWithMarkers.isEmpty()) return;
        
        Map<IFile, List<IEditorInput>> inputMap = EditorUtils.getOpenEditors();
        
        for (IResource file : filesWithMarkers) {
            for (IEditorInput input : inputMap.getOrDefault(file, emptyList())) {
                for (IWorkbenchWindow workbenchWindow : PlatformUI.getWorkbench().getWorkbenchWindows()) {
                    IEditorReference[] editorRefs = workbenchWindow.getActivePage()
                        .findEditors(input, null, IWorkbenchPage.MATCH_INPUT);
                    
                    for (IEditorReference ref : editorRefs) {
                        IEditorPart part = ref.getEditor(false);

                        if (part != null && part instanceof ITextEditor) {
                            MarkerRegionUpdater.updateMarkerPositions(file, (ITextEditor) part);
                        }
                    }
                }
            }
        }
    }
    
    
    /**
     * Sets the information in match to markser.
     */
    public static void fillMarker(ProblemMatch match, IMarker marker) throws CoreException {
        if (match.severity != null) {
            marker.setAttribute(IMarker.SEVERITY, match.severity.eclipseValue);
        }
        
        if (match.message != null) {
            marker.setAttribute(IMarker.MESSAGE, match.message);
        }
        
        if (match.line != null)  {
            marker.setAttribute(IMarker.LINE_NUMBER, match.line);
            if (match.column != null) {
                marker.setAttribute(BuildMarkerPlugin.MARKER_COLUMN_ATTR, match.column);
                if (match.columnEnd != null) {
                    marker.setAttribute(BuildMarkerPlugin.MARKER_COLUMN_END_ATTR, match.columnEnd);
                    if (match.lineEnd != null) marker.setAttribute(BuildMarkerPlugin.MARKER_LINE_END_ATTR, match.lineEnd);
                }
            }
        }
    }
}