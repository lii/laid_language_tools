package se.lidestrom.laid.build_marker;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.variables.VariablesPlugin;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchesListener2;
import org.eclipse.debug.core.IStreamListener;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.debug.core.model.IStreamMonitor;

import se.lidestrom.laid.build_marker.ParserLoader.ParserCreators;

class ParserLauchConfigData {
    private static final String PARSER_ATTACHED_TAG = BuildMarkerPlugin.PLUGIN_ID + ".PARSER_ATTACHED"; 

    private ILaunchConfiguration launchConfig;
    private ILaunch launch;

    public ParserLauchConfigData(ILaunch launch) {
        this.launch = launch;
        this.launchConfig = launch.getLaunchConfiguration();
    }

    public ParserLauchConfigData(ILaunchConfiguration launchConfig) {
        this.launch = null;
        this.launchConfig = launchConfig;
    }
    
    public boolean isParserEnabled() throws CoreException {
        return launchConfig.getAttribute(BuildMarkerPlugin.PARSER_ENABLED_ATTR, true);
    }
    
    public boolean isParserAttached() {
        return launch.getAttribute(PARSER_ATTACHED_TAG) != null;        
    }
    
    public void setAttachedTag() {
        // Set tag that shows that this launch already have parsers attached
        launch.setAttribute(PARSER_ATTACHED_TAG, "ATTACHED");
    }
    
    public String getParserConfigFileName() throws CoreException {
        return launchConfig.getAttribute(
            BuildMarkerPlugin.PARSER_CONFIG_FILE_ATTR, (String) null);
    }
    
    public Optional<String> getParserFilePrefix() throws CoreException {
        return Optional.ofNullable(launchConfig.getAttribute(
            BuildMarkerPlugin.PARSER_FILE_PREFIX_ATTR, (String) null));
    }
}

/**
 * Attaches parsers as listeners on process build output for launches which have a parser
 * configured in their launch config.
 * <p/>
 * Called from a launch listener.
 */
public class ParserAttacher {
    private static final String PARSER_ATTACHED_TAG = BuildMarkerPlugin.PLUGIN_ID + ".PARSER_ATTACHED"; 
 
    public static void attachParsers(ILaunch launch) {
        try {
            // First check if parsers are enabled on this lauch 
            ILaunchConfiguration launchConfig = launch.getLaunchConfiguration();
            
            if (launchConfig == null || launchConfig.getFile() == null) {
                return;
            }
            
            
            // Ugly way to get the project. Is there a better one? Fails if config file is outside project.
            // But maybe its okay because builder entry in .project can't refer to outside of project.
            // 
            // The following works but gives an illegal access warning: ExternalToolBuilder.getBuildProject().
            // It also doesn't work for non-builders.
            IContainer project = launchConfig.getFile().getProject();
            
            boolean clearAllMarkers = launchConfig.getAttribute(BuildMarkerPlugin.CLEAR_ALL_MARKERS_ATTR, false);

            if (clearAllMarkers) {
                project.deleteMarkers(BuildMarkerPlugin.MARKER_ID, true, IResource.DEPTH_INFINITE);
            }
            
            if (launch.getAttribute(PARSER_ATTACHED_TAG) != null ||
                !launchConfig.getAttribute(BuildMarkerPlugin.PARSER_ENABLED_ATTR, false)) {
                return;
            }

            // Set tag that shows that this launch already have parsers attached
            launch.setAttribute(PARSER_ATTACHED_TAG, "ATTACHED");

            String parserConfigFileName = launchConfig.getAttribute(
                BuildMarkerPlugin.PARSER_CONFIG_FILE_ATTR, (String) null);
            
            if (parserConfigFileName == null) {
                throw new IllegalStateException("Launch config has parser enabled attribute but no parser config file attribute");
            }
            
            // TODO: Project not necessary
            ParserConfig parserConfig = ParserConfig.loadFromFile(parserConfigFileName, project);
            String filePrefix = VariablesPlugin.getDefault().getStringVariableManager().performStringSubstitution(
                launchConfig.getAttribute(BuildMarkerPlugin.PARSER_FILE_PREFIX_ATTR, parserConfig.filePrefix.orElse("")));

            // This ID is set on all markers created for this builder
            // TODO: Project should not be necessary, since markers are placed on project
            String builderId = project.getName() + "/" + launchConfig.getName() + "/" + parserConfigFileName;

            if (!clearAllMarkers) {
                deleteMarkersWithId(project, builderId);
            }
            
            // TODO: Project not necessary
            ParserCreators parserCreators = BuildMarkerPlugin.getDefault().getParserLoader().getParsers(parserConfig, project);
            List<OutputParserStreamListener> streamListeners = new ArrayList<>();
            
            // Add listener to those out streams for which there is listeners
            for (IProcess p : launch.getProcesses()) {
                if (!parserCreators.stdoutCreators.isEmpty()) {
                    // TODO: Project not necessary
                    OutputParserStreamListener l = new OutputParserStreamListener(
                        parserCreators.stdoutCreators.stream().map(s -> s.get()).collect(toList()), 
                        project, builderId, filePrefix);
                    addAndNotifyStreamListener(p.getStreamsProxy().getOutputStreamMonitor(), l);
                    streamListeners.add(l);
                }
                
                if (!parserCreators.stderrCreators.isEmpty()) {
                    OutputParserStreamListener l = new OutputParserStreamListener(
                        parserCreators.stderrCreators.stream().map(s -> s.get()).collect(toList()),
                        project, builderId, filePrefix);
                    addAndNotifyStreamListener(p.getStreamsProxy().getErrorStreamMonitor(), l);
                    streamListeners.add(l);
                }
            }

            addTeminationListener(streamListeners);

        } catch (CoreException e) {
            throw new RuntimeException(e);
        }
    }
    
    /** Deletes all build markers with the given builderId from project. */
    private static void deleteMarkersWithId(IContainer project, String builderId) throws CoreException {
        IMarker[] oldMarkers = project.findMarkers(BuildMarkerPlugin.MARKER_ID, true, IResource.DEPTH_INFINITE);
        for (IMarker marker : oldMarkers) {
            if (builderId.equals(marker.getAttribute(BuildMarkerPlugin.MARKER_BUILDER_NAME_ATTR))) {
                marker.delete();
            }
        }
    }

    /** Attaches a listener which makes a final call to the parsers. */
    private static void addTeminationListener(List<OutputParserStreamListener> streamListeners) {
        DebugPlugin.getDefault().getLaunchManager().addLaunchListener(new ILaunchesListener2() {
            
            private void buildTerminated() {
                try {
                    Utils.forEachSupressExceptions(
                        streamListeners, Exception.class, RuntimeException::new, 
                        OutputParserStreamListener::buildTerminated);
                } finally {
                    DebugPlugin.getDefault().getLaunchManager().removeLaunchListener(this);
                }
            }
            
            @Override public void launchesRemoved(ILaunch[] launches) {
                buildTerminated();
            }
            @Override public void launchesTerminated(ILaunch[] launches) {
                buildTerminated();
            }

            @Override public void launchesChanged(ILaunch[] launches) {}
            @Override public void launchesAdded(ILaunch[] launches) {}
        });
    }
    
    
    /**
     * Adds listener to monitor, and calls listener with any content monitor already has.
     * NOTE: This methods synchronises on monitor while listener is called. Listener may
     * not wait on any thread that waits for monitors monitor, what would result in dead-lock.
     */
    private static void addAndNotifyStreamListener(IStreamMonitor monitor, IStreamListener listener) {
        // Synchronise on monitor to prevent writes to stream while we are adding listener.
        // It's weird to synchronise on monitor because that's a shared object, but that's 
        // what ProcessConsole does.  
        synchronized (monitor) {
            String contents = monitor.getContents();
            if (!contents.isEmpty()) {
                // Call to unknown code while synchronising on monitor. This is dead-lock prone!
                // Listener must not wait for other threads that are waiting in line to 
                // synchronise on monitor.
                listener.streamAppended(contents, monitor);
            }
            monitor.addListener(listener);
        }
    }
}

