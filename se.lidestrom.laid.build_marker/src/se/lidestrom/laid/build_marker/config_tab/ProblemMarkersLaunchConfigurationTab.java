package se.lidestrom.laid.build_marker.config_tab;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import se.lidestrom.laid.build_marker.BuildMarkerPlugin;

/**
 * Implements to GUI for the Program builder dialog. It enables the user to set
 * for example parser config file location. Information is saved in the launch configuration.
 */
public class ProblemMarkersLaunchConfigurationTab extends AbstractLaunchConfigurationTab {

    private Button enableParsersButton;
    private Text parserFileLoc;
    private Text filePrefix;
    private Button clearMarkersButton;
    
    private boolean isInitializing = false;
    private List<Control> parserDataControls;

    @Override
    public void createControl(Composite parent) {

        Composite container = new Composite(parent, SWT.NONE);
        setControl(container);

        container.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, true, true, 1, 1));
        container.setLayout(new GridLayout(2, false));

        /******************/

        // Manually line break into multiple Labels. Ugly hack because I don't get text wrapping to work.
        Arrays.asList(
            "The Laid Build Marker plug-in is a build output problem parser and marker. It can add output", 
            "parsers to builders that can detect error output and add problem markers to editors and views.")
            .forEach(l -> {
                    Label description = new Label(container, SWT.WRAP);
                    description.setText(l);
                    description.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));        
                });

        Label sep = new Label(container, SWT.SEPARATOR | SWT.HORIZONTAL);
        sep.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));

        parserDataControls = new ArrayList<>();
        
        /*******************/
        
        clearMarkersButton = new Button(container, SWT.CHECK); 
        clearMarkersButton.setText("Clear all problem markers before run");
        clearMarkersButton.setToolTipText(
            "This option makes the parser clear all markers from the Build Marker plug-in, not just "
            +"from this builder which is the default. This is useful for clean operations.");
        clearMarkersButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
        clearMarkersButton.addListener(SWT.Selection, _e -> onChange());

        /*******************/

        enableParsersButton = new Button(container, SWT.CHECK); 
        enableParsersButton.setText("Enable build output error parser");
        enableParsersButton.setToolTipText("Decides whether the configured parser should be used or not");
        enableParsersButton.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 2, 1));
        enableParsersButton.addListener(SWT.Selection, _e -> onChange());
        enableParsersButton.addListener(SWT.Selection, _e -> updateControlEnabled());
                
        /******************/

        Label parserLocationLabel = new Label(container, SWT.NONE);
        parserLocationLabel.setText("Parser config file location:");
        parserLocationLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        
        parserFileLoc = new Text(container, SWT.CHECK);
        parserFileLoc.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        parserFileLoc.setToolTipText(
            "Sets the absolute path to the parser configuration file. Supports variables, "
            + "for example ${project_loc}/some_parser.xml will refer to a file in the current project.");
        parserFileLoc.addModifyListener(_e -> onChange());
        parserDataControls.add(parserFileLoc);

        /******************/

        Label filePrefixLabel = new Label(container, SWT.NONE);
        filePrefixLabel.setText("Parser output file prefix:");
        filePrefixLabel.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1));
        
        filePrefix = new Text(container, SWT.CHECK); 
        filePrefix.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
        filePrefix.setToolTipText(
            "Sets the file prefix which is added to every resource path that a parser returns. "
            + "It makes it possible to use parsers which do not return the full path of resources. "
            + "Resources are resolved first as build project relative, then as absolute file system "
            + "paths. Supports variables, for example ${container_loc} will refer to the directory of the current file.");
        filePrefix.addModifyListener(_e -> onChange());
        parserDataControls.add(filePrefix);
        
        
        /******************/
        
    }

    private void updateControlEnabled() {
        parserDataControls.forEach(c -> c.setEnabled(enableParsersButton.getSelection()));
    }
    
    private void onChange() {
        if (!isInitializing) {
            // This updates the Apply and Revert buttons
            updateLaunchConfigurationDialog();
        }
    }

    @Override
    public void initializeFrom(ILaunchConfiguration config) {
        try {
            isInitializing = true;
            clearMarkersButton.setSelection(config.getAttribute(BuildMarkerPlugin.CLEAR_ALL_MARKERS_ATTR, false));
            parserFileLoc.setText(config.getAttribute(BuildMarkerPlugin.PARSER_CONFIG_FILE_ATTR, ""));
            filePrefix.setText(config.getAttribute(BuildMarkerPlugin.PARSER_FILE_PREFIX_ATTR, ""));
            // This must go last because it triggers listener
            enableParsersButton.setSelection(config.getAttribute(BuildMarkerPlugin.PARSER_ENABLED_ATTR, false));
            updateControlEnabled();
            isInitializing = false;
        } catch (CoreException exc) {
            throw new RuntimeException(exc);
        }
    }
    
    @Override
    public void performApply(ILaunchConfigurationWorkingCopy config) {
        config.setAttribute(BuildMarkerPlugin.CLEAR_ALL_MARKERS_ATTR, clearMarkersButton.getSelection());
        config.setAttribute(BuildMarkerPlugin.PARSER_ENABLED_ATTR, enableParsersButton.getSelection());
        config.setAttribute(BuildMarkerPlugin.PARSER_CONFIG_FILE_ATTR, parserFileLoc.getText());
        config.setAttribute(BuildMarkerPlugin.PARSER_FILE_PREFIX_ATTR, filePrefix.getText());
    }

    @Override
    public void setDefaults(ILaunchConfigurationWorkingCopy config) {
        config.setAttribute(BuildMarkerPlugin.CLEAR_ALL_MARKERS_ATTR, false);
        config.setAttribute(BuildMarkerPlugin.PARSER_ENABLED_ATTR, false);
        config.setAttribute(BuildMarkerPlugin.PARSER_CONFIG_FILE_ATTR, "${build_project}/example_parser.xml");
        config.setAttribute(BuildMarkerPlugin.PARSER_FILE_PREFIX_ATTR, "${container_path}");
    }

    @Override
    public String getName() {
        return "Problem Markers";
    }
}
