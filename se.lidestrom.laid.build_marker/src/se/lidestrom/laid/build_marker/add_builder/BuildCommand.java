package se.lidestrom.laid.build_marker.add_builder;

import static java.util.stream.Collectors.toMap;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.core.externaltools.internal.model.BuilderCoreUtils;
import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IncrementalProjectBuilder;

import se.lidestrom.editor_util.EditorUtils;
import se.lidestrom.editor_util.FunctionUtils;

/**
 * This class contains the information from a buildCommand entry in a .project file.
 */
@SuppressWarnings("restriction")
class BuildCommand {
    
    static private class Argument {
        public String key;
        public String value;
    }

    @XmlElement
    public String importInfo;
    
    // Don't use a Map here because entries must be named "dictionary"
    @XmlElementWrapper(name = "arguments")
    @XmlElement(name = "dictionary", required = true)
    public final List<Argument> arguments;
    
    @XmlElement(required = true)
    public final String name;
    
    @XmlElement(required = true)
    public final String triggers;

    // For XML lib
    public BuildCommand() {
        this.name = null;
        this.triggers = null;
        this.arguments = null;
    }
    
    public String getBuildConfigHandler() {
        for (Argument a : arguments) {
            if (a.key.equals(BuildCommand.LAUNCH_CONFIG_HANDLE_KEY)) {
                return a.value;
            }
        }
        throw new IllegalStateException();
    }

    public ICommand createCommand(IProjectDescription d) {
        ICommand c = d.newCommand();
        
        c.setBuilderName(name);
        c.setArguments(arguments.stream().collect(toMap(a -> a.key, a -> a.value)));

        // Build kinds a set to true on creation, must clear them
        triggerNameToBuildKind.values().forEach(k -> c.setBuilding(k, false));
        Arrays.asList(triggers.split(","))
            .forEach(s -> c.setBuilding(triggerNameToBuildKind.get(s), true));
        
        return c;
    }
    
    public static final String LAUNCH_CONFIG_HANDLE_KEY = BuilderCoreUtils.LAUNCH_CONFIG_HANDLE;
    
    private static final Map<String, Integer> triggerNameToBuildKind = EditorUtils.newLinkedHashMap(
        "full"        , IncrementalProjectBuilder.FULL_BUILD,
        "incremental" , IncrementalProjectBuilder.INCREMENTAL_BUILD,
        "auto"        , IncrementalProjectBuilder.AUTO_BUILD,
        "clean"       , IncrementalProjectBuilder.CLEAN_BUILD);

    
    public static List<BuildCommand> readConfigFile(Path in) throws JAXBException {
        Unmarshaller unmarshaller = FunctionUtils.runtimize(
            () -> JAXBContext.newInstance(BuildCommands.class).createUnmarshaller());
        return ((BuildCommands) unmarshaller.unmarshal(in.toFile())).buildCommands;
    }
    
    @XmlRootElement
    static class BuildCommands {
        @XmlElement(name = "buildCommand", required = true)
        public List<BuildCommand> buildCommands = new ArrayList<>();
    }
}
