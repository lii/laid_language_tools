package se.lidestrom.laid.build_marker.add_builder;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.function.Supplier;

import javax.xml.bind.JAXBException;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.runtime.CoreException;

import se.lidestrom.editor_util.FunctionUtils;

public class BuilderAdder {
    
    private static final String EXTERNAL_TOOL_BUILDERS_DIR = ".externalToolBuilders";
    private static final String PROJECT_PREFIX = "<project>/";
    
    private static class SourceAndTarget {
        final Path source;
        final IFile target;

        public SourceAndTarget(Path launchConfgigSource, IFile launchConfgigTarget) {
            this.source = launchConfgigSource;
            this.target = launchConfgigTarget;
        }
    }
    
    public static List<String> addProgramBuilder(IProject project, Path builderFile, 
        Predicate<IFile> overwriteLaunchConfigConfirmer,
        Predicate<String> buildOverwriteConfirmer
        ) throws ExecutionException, IOException, CoreException, JAXBException {
        // Parse the selected the build entry file for target path
        List<BuildCommand> buildCommands = BuildCommand.readConfigFile(builderFile);
        
        List<SourceAndTarget> sourceAndTargets = buildCommands.stream()
            .map(c -> c.getBuildConfigHandler())
            .map(FunctionUtils.runtimizing(
                launchConfPathStr -> createSourceAndTargetPaths(launchConfPathStr, builderFile, project)))
            .filter(s -> s != null)
            .collect(toList());
            
        if (sourceAndTargets.isEmpty()) return emptyList();

        // Copy the build launch config files
        for (SourceAndTarget p : sourceAndTargets) {
            
            Supplier<InputStream> is = FunctionUtils.runtimizing(() -> Files.newInputStream(p.source));

            if (p.target.exists()) {
                if (overwriteLaunchConfigConfirmer.test(p.target)) {
                    p.target.setContents(is.get(), true, false, null);
                }
            } else {
                createFolderAndParents(p.target.getParent());
                p.target.create(is.get() , false, null);
            }
        }

        List<String> infoMessages = new ArrayList<>();
        
        // Add the build commands to the project 
        for (BuildCommand buildCommand : buildCommands) {
            // Show info from file is this info has not already been showed
            if (buildCommand.importInfo != null) {
                infoMessages.add(buildCommand.importInfo);
            }
            
            addBuilderToProject(buildCommand, project, buildOverwriteConfirmer);
        }
        
        return infoMessages;
    }
    
    /**
     * Create source and target paths.
     */
    private static SourceAndTarget createSourceAndTargetPaths(String launchConfPathStr, Path builderFile, IProject project) throws ExecutionException {
        String launchConfigNoPrefix = launchConfPathStr.replaceFirst(PROJECT_PREFIX, "");

        IFile launchConfigTarget = launchConfPathStr.startsWith(PROJECT_PREFIX)
            ? project.getFile(launchConfigNoPrefix)
            : project.getFolder(EXTERNAL_TOOL_BUILDERS_DIR).getFile(launchConfigNoPrefix);
            
        Path launchConfigSource = builderFile.getParent().resolve(Paths.get(launchConfigNoPrefix).getFileName());
        
        return new SourceAndTarget(launchConfigSource, launchConfigTarget);
    }
    
    private static void addBuilderToProject(BuildCommand buildCmd, IProject project, Predicate<String> confirmer) throws CoreException {
        IProjectDescription projDesc = project.getDescription();
        
        List<ICommand> commands = new ArrayList<>(Arrays.asList(projDesc.getBuildSpec()));

        boolean foundOld = false;
        
        // Checks if command with this path exists, overwrite it if confirmer says so
        for (int i = 0; i < commands.size(); i++) {
            ICommand c = commands.get(i);
            
            if (Objects.equals(c.getArguments().get(BuildCommand.LAUNCH_CONFIG_HANDLE_KEY),
                buildCmd.getBuildConfigHandler())) {
                foundOld = true;
                if (confirmer.test(buildCmd.getBuildConfigHandler())) {
                    commands.set(i, buildCmd.createCommand(projDesc));
                }
            }
        }
        
        if (!foundOld) {
            commands.add(buildCmd.createCommand(projDesc));
        }
        
        projDesc.setBuildSpec(commands.toArray(new ICommand[0]));
        
        project.setDescription(projDesc, null);
    }
    
    private static void createFolderAndParents(IContainer folder) throws CoreException {
        if (!folder.exists()) {
            createFolderAndParents(folder.getParent());
            // This assumes a non-existing container should be a folder
            ((IFolder) folder).create(false, false, null);
        }
    }
}
