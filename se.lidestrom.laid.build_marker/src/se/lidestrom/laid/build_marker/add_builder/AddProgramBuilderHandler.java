package se.lidestrom.laid.build_marker.add_builder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.xml.bind.JAXBException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.URIUtil;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import se.lidestrom.editor_util.EditorUtils;
import se.lidestrom.editor_util.FunctionUtils;
import se.lidestrom.laid.build_marker.BuildMarkerPlugin;

/**
 * Implements the Add Program Builder command. Opens a file dialog and lets to user choose a
 * builder entry file. Adds the builder in this file to the project, and copies a builder
 * launch config it refers to into the project.
 */
public class AddProgramBuilderHandler extends AbstractHandler {

    private static Set<String> showedInfoTexts = new HashSet<>();
    private static final String BUNDLE_PARSER_ENTRY_DIR = "res/parser_entries/";
    private static FileDialog chooseBuilderFileDialog;

    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {
        IProject project = EditorUtils.getSelectedProject(HandlerUtil.getCurrentSelection(event))
            .orElseThrow(() -> new ExecutionException("No project selected"));

        try {
            // First get path from the user
            Path builderFile = queryBuilderEntryFile().orElse(null);
            if (builderFile == null) return null;
            
            List<String> infoMessages = BuilderAdder.addProgramBuilder(project, builderFile, 
                AddProgramBuilderHandler::confirmOverwriteLaunchConfig,
                AddProgramBuilderHandler::confirmBuilderOverwrite);

            infoMessages.stream().filter(showedInfoTexts::add)
                .forEach(info -> MessageDialog.openInformation(null, "Info", info));
        
        } catch (IOException | CoreException | JAXBException exc) {
            throw new ExecutionException("Error in " + getClass().getSimpleName(), exc);
        }
        
        return null;
    }

    /**
     * Queries the user for a builder entry file and returns it, or null if none where selected.
     */
    private static Optional<Path> queryBuilderEntryFile() throws ExecutionException {
        if (chooseBuilderFileDialog == null) {
            chooseBuilderFileDialog = new FileDialog(
                PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());
            chooseBuilderFileDialog.setFilterExtensions(new String[] { "*.xml", "*.*" });
            
            chooseBuilderFileDialog.setText("Select Build Entry File");
            
            // Get parser path as platform dependent string. Ex: C:\\path\\parsers
            String parserEntryPath = FunctionUtils.runtimize(() -> 
                new File(URIUtil.toURI(FileLocator.toFileURL(BuildMarkerPlugin.getDefault().getBundle()
                    .getEntry(BUNDLE_PARSER_ENTRY_DIR)))).toString());
            
            chooseBuilderFileDialog.setFilterPath(parserEntryPath);
        }
        
        String builderFileStr = chooseBuilderFileDialog.open();
        if (builderFileStr == null) return Optional.empty();
        
        Path builderFile = Paths.get(builderFileStr);
        
        if (!Files.exists(builderFile)) {
            throw new ExecutionException("Bundle file does not exist: " + builderFile);
        }
        
        return Optional.of(builderFile);
    }
    
    
    private static boolean confirmOverwriteLaunchConfig(IFile target) {
        MessageDialog confirmOverwriteDialog = new MessageDialog(
            null, "Confirm", null, "The launch configuration with the following file name already exist. Overwrite the existing one or skip copying new one?\n\n" + target,
            MessageDialog.QUESTION,
            new String[] {"Overwrite existing file", "Skip this file"},
            0);
            
        return confirmOverwriteDialog.open() != 0;
    }

    private static boolean confirmBuilderOverwrite(String filePath) {
        MessageDialog dialog = new MessageDialog(
            null, "Confirm", null, 
            "A build command with the following builder configuration file already exists. Should the existing build command entry in .project and the builder configuration file be overwritten?\n\n" + filePath,
            MessageDialog.QUESTION,
            new String[] {"Overwrite existing builder", "Skip this builder"},
            0);
        
        return dialog.open() == 0;
    }
}
