package se.lidestrom.laid.build_marker;

import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchListener;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The entry point plugin class. Registers to listeners and and contains some utility methods.
 */
public class BuildMarkerPlugin extends AbstractUIPlugin implements IStartup {

    public static final String MARKER_BUILDER_NAME_ATTR = "se.lidestrom.laid.build_marker.attribute.builder_id";
    
    public static final String MARKER_COLUMN_ATTR = "se.lidestrom.laid.build_marker.attribute.column";
    public static final String MARKER_COLUMN_END_ATTR = "se.lidestrom.laid.build_marker.attribute.column_end";
    public static final String MARKER_LINE_END_ATTR = "se.lidestrom.laid.build_marker.attribute.line_end";
    
    public static final String PLUGIN_ID = "se.lidestrom.laid.build_marker"; //$NON-NLS-1$
	public static final String MARKER_ID = PLUGIN_ID + ".marker";
	public static final String PARSERS_EXTENSION_POINT_ID = PLUGIN_ID + ".parsers";

    public static final String PARSER_CONFIG_FILE_ATTR = BuildMarkerPlugin.PLUGIN_ID + ".PARSER_FILE_ATTR"; 
    public static final String PARSER_ENABLED_ATTR = BuildMarkerPlugin.PLUGIN_ID + ".PARSER_ENABLED_ATTR";
    public static final String PARSER_FILE_PREFIX_ATTR = BuildMarkerPlugin.PLUGIN_ID + ".PARSER_FILE_PREFIX_ATTR";
    public static final String CLEAR_ALL_MARKERS_ATTR = BuildMarkerPlugin.PLUGIN_ID + ".CLEAR_ALL_MARKERS_ATTR";

    private static BuildMarkerPlugin plugin;
	
    /**
     * Used to attach parsers to lauch objects. 
     */
	private final ILaunchListener parserAttacherLauchListener = new ILaunchListener() {
	        @Override
	        public void launchChanged(ILaunch launch) {
	            ParserAttacher.attachParsers(launch);
	        }

	        @Override public void launchAdded(ILaunch launch) {}
	        @Override public void launchRemoved(ILaunch launch) {}
	    };
	
	/**
	 * A possible selection of streams. One or both of the normal output streams.
	 */
	public enum StreamSelection {
        STDOUT, STDERR, BOTH
    }
	
	@Override
    public void start(BundleContext context) throws Exception {
	    super.start(context);
		plugin = this;
        DebugPlugin.getDefault().getLaunchManager().addLaunchListener(parserAttacherLauchListener);

        PlatformUI.getWorkbench().addWindowListener(MarkerRegionUpdater.getWindowListener());
	}

	@Override
    public void stop(BundleContext context) throws Exception {
        DebugPlugin.getDefault().getLaunchManager().removeLaunchListener(parserAttacherLauchListener);
		plugin = null;
		super.stop(context);
	}

	public static BuildMarkerPlugin getDefault() {
		return plugin;
	}

	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}

	
    @Override
    public void earlyStartup() {
        // Called on startup. But plugin is loaded before this is called so initialisation
        // is done in start(...) instead.
    }
    
    public static void logWarning(String msg) {
        log(Status.WARNING, msg);
    }
    
    public static void logError(String msg) {
        log(Status.ERROR, msg);
    }
    
    public static void log(int status, String msg) {
        getDefault().getLog().log(new Status(status, PLUGIN_ID, msg));
    }
    
    public ParserLoader getParserLoader() {
        return new ParserLoader();
    }
    
}


